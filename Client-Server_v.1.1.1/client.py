import clientlib

HOST = "127.0.0.1"
PORT = 33000
BUFFER = 1024

client = clientlib.Client(HOST, PORT, BUFFER)

while True:
    client.set_connection()
    client.send_request()