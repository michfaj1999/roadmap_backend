import serverlib

HOST = '127.0.0.1'
PORT = 33000
BUFFER = 1024

server = serverlib.Server(HOST, PORT, BUFFER)

server.set_listening_socket()

while not server.close_flag:
    server.accept_client_socket()
    server.serve_request()