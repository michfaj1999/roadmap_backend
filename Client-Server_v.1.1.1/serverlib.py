import time
from datetime import datetime
import socket as s
import sys
import json


class Server:

    def __init__(self, HOST, PORT, BUFFER):
        self.HOST = HOST
        self.PORT = PORT
        self.BUFFER = BUFFER
        self.lifetime_start = time.time()
        self.start_date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        self.server_version = "v.1.1.1"
        self.close_flag = False
        self.commands = ("uptime", "info", "help", "close")
    
    def uptime(self):
        live_time_stop = time.time()
        live_time = live_time_stop - self.lifetime_start

        uptime = {
            "time": str(round(live_time, 2)),
        }
        return json.dumps(uptime, ensure_ascii=False).encode("utf8")

    def help(self):
        commands = {
            "uptime": "returns server lifetime",
            "info": "return server version and start date",
            "close": "close connection with server"
        }
        return json.dumps(commands, ensure_ascii=False).encode("utf8")
    
    def info(self):
        server_info = {
            "server_version": self.server_version,
            "start_date": str(self.start_date),
        }
        return json.dumps(server_info, ensure_ascii=False).encode("utf8")

    def set_listening_socket(self):
        print("Server starting...")
        print()
        self.listening_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.listening_socket.bind((self.HOST, self.PORT))
        self.listening_socket.listen()

    def accept_client_socket(self):
        self.client_socket, self.adress = self.listening_socket.accept()
        print(f"Connected to :{self.adress[0]} : {self.adress[1]} ")
        print() 

    def serve_request(self):
        request = self.client_socket.recv(self.BUFFER).decode("utf8")
        if request in self.commands:
            match request:
                case "uptime":
                    msg = self.uptime()
                    self.client_socket.send(msg)
                    self.client_socket.close()
                    print("Response send")
                case "info":
                    msg = self.info()
                    self.client_socket.send(msg)
                    self.client_socket.close()
                    print("Response send")
                case "help":
                    msg = self.help()
                    self.client_socket.send(msg)
                    self.client_socket.close()
                    print("Response send")
                case "close":
                    self.client_socket.send(b"close")
                    self.client_socket.shutdown(s.SHUT_RDWR)
                    self.client_socket.close()
                    print("Closing the server...")
                    sys.exit(0)

        else:
            self.client_socket.send(
                b"invalid request.Type help to see server commands"
                )
            self.client_socket.close()                        
        
    
