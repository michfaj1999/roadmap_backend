import psycopg2
import config
from threading import Lock
import schedule
import time
from datetime import timedelta, datetime


class ThreadSafeList:
    def __init__(self):
        self._list = list()
        self._lock = Lock()
 
    def append(self, value):
        with self._lock:
            self._list.append(value)

    def pop(self, value=None):
        if value is None:
            with self._lock:
                return self._list.pop()
        else:
            with self._lock:
                return self._list.pop(value)

    def get(self, index):
        with self._lock:
            return self._list[index]
    
    def length(self):
        with self._lock:
            return len(self._list)
 
    def remove(self, value):
        with self._lock:
            return self._list.remove(value)

    def slice_list(self, left_range=0, right_range=0):
        with self._lock:
            self._list = self._list[left_range:right_range + 1]

    def __iter__(self):
        self.i = -1
        return self

    def __next__(self):
        with self._lock:
            if self.i < len(self._list)-1:
                self.i += 1
                return self._list[self.i]
            else:
                raise StopIteration


class Connection:
    def __init__(self, configfile, section):
        self.connection = self.create_connection(configfile, section)
        self.creation_time = datetime.now()

    def create_connection(self, configfile, section):
        params = config.config(configfile, section)
        try:
            connection = psycopg2.connect(
             **params
            )
            print("Connection successfull")
            return connection
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return None


class Connection_Pool(Connection):
    def __init__(self, min_connections, max_connections, configfile, section, timeout, max_lifetime, idle_lifetime):
        self.min_connections = min_connections
        self.max_connections = max_connections
        self.available_connections = ThreadSafeList()
        self.connections_in_use = ThreadSafeList()
        self.configfile = configfile
        self.section = section
        self.lock = Lock()
        self.timeout = float(timeout)
        self.max_lifetime = timedelta(minutes=max_lifetime)
        self.idle_lifetime = timedelta(minutes=idle_lifetime)
        for _ in range(self.min_connections):
            self.add_connection_to_available()
        schedule.every(1).minute.do(self.manage_connections)
    
    def get_connection(self):
        start_time = time.time()
        while time.time() - start_time < self.timeout:
            if self.available_connections.length() > 0:
                connection = self.move_available_connection_to_used() 
                return connection
            else:
                if self.available_connections.length() + self.connections_in_use.length() < self.max_connections:
                    print("No available connections.Adding another connection.")
                    self.add_connection_to_available()
                    connection = self.move_available_connection_to_used()
                    return connection
                else:
                    print("Too many connections in use.Waiting for an available connection")
                    time.sleep(1)
        print("Connection timeout reached. Unable to acquire connection.")
        return None
            
    def add_connection_to_available(self):
        connection = Connection(self.configfile, self.section,)
        idle_time = datetime.now()
        if connection.connection is None:
            return
        self.available_connections.append((connection, idle_time))
    
    def move_available_connection_to_used(self):
        connection_tuple = self.available_connections.pop(0)
        connection, idle_time = connection_tuple
        self.connections_in_use.append(connection)
        return connection
    
    def release_connection(self, connection):
        self.connections_in_use.remove(connection)
        idle_time = datetime.now()
        self.available_connections.append((connection, idle_time))
    
    def destroy_unused_connections(self):
        print("checking for too many unused connections")
        now = datetime.now()
        with self.lock:
            while self.available_connections.length() > self.min_connections :
                connection_tuple = self.available_connections.get(0)
                connection, idle_time = connection_tuple
                if (now - connection.creation_time).total_seconds() > self.max_lifetime.total_seconds():
                    connection.connection.close()
                    self.available_connections.remove(connection_tuple)
                    
                    print("Connection closed due to max lifetime")
                if (now - idle_time).total_seconds() > self.idle_lifetime.total_seconds():
                    self.available_connections.remove(connection_tuple)
                    print("Connection closed due to idle")
                else:
                    break

    def run_health_check(self):
        print("Running health check for connections.")
        with self.lock:
            for connection_tuple in self.available_connections:
                connection, _ = connection_tuple
                if not self.is_connection_valid(connection):
                    print("Connection failed health check:", connection)
                    connection.connection.close()
                    self.available_connections.remove(connection_tuple)
                    self.add_connection_to_available()
                    
    def is_connection_valid(self, connection):
        try:
            with connection.connection.cursor() as cursor:
                cursor.execute("SELECT 1")
                return True
        except (Exception, psycopg2.Error) as error:
            print("Error during health check:", error)
            return False
          
    def manage_connections(self):
        self.destroy_unused_connections()
        self.run_health_check()

    def run_scheduler(self):
        print("Manage connection scheduler on")
        while True:
            schedule.run_pending()
            time.sleep(1)

    def stop_scheduler(self):
        print("Scheduler stopped")