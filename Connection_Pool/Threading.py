from Connection_Pool import Connection_Pool
import threading
import time
import random


connection_pool = Connection_Pool(max_connections=97, min_connections=5, configfile="connection_pool.ini", section="database",timeout=30, max_lifetime=2, idle_lifetime=1)

Lock = threading.Lock()

scheduler_thread = threading.Thread(target=connection_pool.run_scheduler)
scheduler_thread.daemon = True
scheduler_thread.start()


def elementary_thread(Lock):
    with Lock:
        connection = connection_pool.get_connection()  
    if connection is None:
        return
    time.sleep(random.randint(0, 10))
    connection_pool.release_connection(connection)

No_of_reps = 0  
time_sequence = [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1]
while No_of_reps < 50:
    print("REPETITION:", No_of_reps)
    print("available connections:", connection_pool.available_connections.length())
    print("Connections in use:", connection_pool.connections_in_use.length())
    for i in range(random.randint(1, 200)):
        time.sleep(random.choice(time_sequence))
        thread = threading.Thread(target=elementary_thread, args=[Lock,])
        thread.start()
    No_of_reps += 1
connection_pool.stop_scheduler()  
