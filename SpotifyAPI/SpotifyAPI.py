from UserToken import client_id, client_secret, local_redirect_uri, server_address
import requests
from requests.exceptions import RequestException
import base64
import webbrowser
from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs, quote
import tkinter as tk
from tkinter import ttk
from datetime import datetime


class Spotify:

    def __init__(self, client_id, client_secret, HTTPServer):
        self.client_id = client_id
        self.client_secret = client_secret
        self.authorization_code = None
        self.access_token = None
        self.refresh_token = None
        self.HTTPServer = HTTPServer
        self.access_token_expire_time = None
    
    def encode_client_credentials(self):
        """Encode client credentials for access token.
           Returns:
            bytes object - base64 encoded client credentials
        """
        return base64.b64encode(f"{self.client_id}:{self.client_secret}".encode()).decode('utf-8')

    def get_access_and_refresh_token(self):
        "send request for access and refresh token"
        client_credentials = self.encode_client_credentials()
        token_url = 'https://accounts.spotify.com/api/token'
        token_params = {
            'grant_type': 'authorization_code',
            'code': self.authorization_code,
            'redirect_uri': local_redirect_uri
        }
        token_headers = {
            'Authorization' : f'Basic {client_credentials}'
        }
        try:
            response = requests.post(token_url, data=token_params, headers=token_headers)
            response.raise_for_status()
            if response.status_code == 200:
                tokens = response.json()
                print("Access token granted")
                self.access_token = tokens['access_token']
                self.refresh_token = tokens['refresh_token']
                self.access_token_expire_time = datetime.now().timestamp() + tokens['expires_in']
                return
            else:
                print("Access token error:".format(response.status_code))
                return
        except RequestException as e:
            print("Error occured while refreshing access token:{}".format(e))
            return

    def get_authorization_code(self):
        "Get authorization code from spotify User"
        authorize_url = 'https://accounts.spotify.com/authorize'
        client_id = self.client_id
        redirect_uri = local_redirect_uri
        scopes = ['user-read-email', 'user-read-private','user-read-playback-state',
                  'user-modify-playback-state']
        auth_url = f"{authorize_url}?client_id={client_id}&response_type=code&redirect_uri={redirect_uri}&scope={' '.join(scopes)}"
        webbrowser.open(auth_url)
        self.HTTPServer.handle_request()
        self.authorization_code =  getattr(self.HTTPServer, 'authorization_code', None)
        self.HTTPServer.server_close()

    def search_for_album(self, album_name):
        """Send request for searching albums by name
           Entries:
           album_name - string - search query related with expected album
           Returns:
           albums - json-encoded response with related albums
        """
        if self.access_token_expired():
            self.refresh_access_token()
        search_url = 'https://api.spotify.com/v1/search'
        request_params = {
            'q': quote(album_name),
            'type':'album'
        }
        request_headers = {
            'Authorization': f'Bearer {self.access_token}'
        }
        try:
            response = requests.get(search_url, headers=request_headers,params=request_params)
            response.raise_for_status()
            if response.status_code == 200:
                albums = response.json()['albums']['items']
                return albums
            elif response.status_code == 401:
                self.refresh_access_token()
                return self.search_for_album(album_name)
        except RequestException as e:
            print("error occured while searching for album:{}".format(e))
            return 

    def play_album(self, album_uri):
        """Send request for changing user's currently played album in user's spotify player
           Entries:
           album_uri - string - URI album to play
        """
        if self.access_token_expired():
            self.refresh_access_token()

        player_endpoint = 'https://api.spotify.com/v1/me/player/play'
        headers = {
            'Authorization': f'Bearer {self.access_token}',
            'Content-Type': 'application/json'
            }
        data = {
            'context_uri': album_uri
            }
        try:
            response = requests.put(player_endpoint,headers=headers,json=data)
            response.raise_for_status()
            if response.status_code == 204:
                print("Starting Album Play...")
                return
            elif response.status_code == 401:
                print("Access Token expired.Refreshing token")
                self.refresh_access_token()
                self.play_album(album_uri)
                return
        except RequestException as e:
            print("Error occured while trying to play album:{}".format(e))
            return
        
    def refresh_access_token(self):
        "Send request for refreshing access token"
        token_url = "https://accounts.spotify.com/api/token"
        token_headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        token_params = {
            'grant_type': 'refresh_token',
            'refresh_token': self.refresh_token,
            'client_id': self.client_id
        }
        try:
            response = requests.post(token_url,headers=token_headers,params=token_params)
            if response.status_code == 200:
                tokens = response.json()
                self.access_token = tokens['access_token']
                self.refresh_token = tokens['refresh_token']
                self.access_token_expire_time = datetime.now().timestamp() + tokens['expires_in']
                return
            elif:
                print("Access token not refreshed.Status code:{}".format(response.status_code))
                return
        except RequestException as e:
            print("Error occured while refreshing access token:{}".format(e))
            return
        
    def access_token_expired(self):
        "Check if access token is still valid"
        if not self.access_token or not self.refresh_token:
            return True
        
        current_time = datetime.now().timestamp()
        return int(current_time) >= self.access_token_expire_time


class CallbackHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        "Catch user's authorization code from browser"
        query_components = parse_qs(urlparse(self.path).query)
        authorization_code = query_components.get('code', [None])[0]

        if authorization_code:
            self.server.authorization_code = authorization_code
            print("Authorization Code found")
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(b"<html><body><h1>Authorization Code Received.This tab will close after 5 seconds.</h1><script>setTimeout(function() { window.close(); }, 5000);</script></body></html>")
        else:
            print("Authorization Code not found in callback")
            self.send_response(400)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(b"Error: Authorization Code not found in callback.")


class SpotifyGUI(tk.Tk):

    def __init__(self, spotify):
        super().__init__()
        self.spotify = spotify
        self.title("Spotify Album Player")
        
        self.album_name_entry = ttk.Entry(self, width=40)
        self.album_name_entry.pack(pady=10)
        
        self.search_button = ttk.Button(self, text="Szukaj", command=self.search_albums)
        self.search_button.pack(pady=5)

        self.albums_listbox = tk.Listbox(self, width=60, height=10)
        self.albums_listbox.pack(pady=10)

        self.play_button = ttk.Button(self, text="Odtwórz", command=self.play_selected_album)
        self.play_button.pack(pady=5)

    def search_albums(self):
        "Display founded albums related with name passed in album name entry"
        album_name = self.album_name_entry.get()
        if album_name:
            albums = self.spotify.search_for_album(album_name)

            self.albums_listbox.delete(0, tk.END)

            for album in albums:
                self.albums_listbox.insert(tk.END, album['name'])

    def play_selected_album(self):
        "Play album selected from albums listbox in user's spotify player"
        selection = self.albums_listbox.curselection()
        if selection:
            album_index = selection[0]
            album_name = self.albums_listbox.get(album_index)

            albums = self.spotify.search_for_album(album_name)
            if albums:
                album_uri = albums[0]['uri']
                self.spotify.play_album(album_uri)


if __name__ == "__main__":
    httpd = HTTPServer(server_address, CallbackHandler)
    spotify = Spotify(client_id, client_secret, httpd)
    spotify.get_authorization_code()
    spotify.get_access_and_refresh_token()
    
    app = SpotifyGUI(spotify)
    app.mainloop()




