import serverlib

HOST = '127.0.0.1'
PORT = 33000
BUFFER = 1024
database = serverlib.Client_Server_DB()
database.create_messages_table()
database.create_users_table()
user_administration = serverlib.UserAdministration(database)
server = serverlib.Server(HOST, PORT, BUFFER, user_administration)
server.set_listening_socket()
server.accept_client_socket()
while not server.close_flag:
    server.serve_request()
database.connection.close()