import messages
import time
from datetime import datetime
import socket as s
import sys
import json
import sqlite3
from sqlite3 import Error


class Server:
    "Class for server functionalities"

    def __init__(self, HOST, PORT, MESSAGE_BUFFER_SIZE, UserAdministration):
        self.HOST = HOST
        self.PORT = PORT
        self.MESSAGE_BUFFER_SIZE = MESSAGE_BUFFER_SIZE
        self.lifetime_start = time.time()
        self.start_date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        self.server_version = "v.1.1.4.2"
        self.close_flag = False
        self.commands = ("uptime", "info", "help", "close", "new", "delete",
                         "login", "messagebox", "message", "deletemsg", "users")
        self.user_administration = UserAdministration

    def uptime(self):
        "returns lifetime of a server"
        live_time_stop = time.time()
        live_time = live_time_stop - self.lifetime_start

        dictionary = {
            "time": str(round(live_time, 2)),
        }
        return json.dumps(dictionary, ensure_ascii=False).encode("utf8")

    def help(self):
        "returns list of commends avalible on the server"
        dictionary = {
            "uptime": "returns server lifetime",
            "info": "return server version and start date",
            "close": "close connection with server",
            "new": "adding new user",
            "login": "log in to your user account",
            "messagebox": "show messagebox",
            "message": "send message to another user",
            "deletemsg": "delete message from you messagebox",
            "users": "show users list"
        }
        return json.dumps(dictionary, ensure_ascii=False).encode("utf8")

    def info(self):
        "returns server version and start_date"
        dictionary = {
            "server_version": self.server_version,
            "start_date": str(self.start_date),
        }
        return json.dumps(dictionary, ensure_ascii=False).encode("utf8")

    def set_listening_socket(self):
        "Setting up listening socket"
        print("Server starting...")
        print()
        self.listening_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.listening_socket.bind((self.HOST, self.PORT))
        self.listening_socket.listen()

    def accept_client_socket(self):
        "Accepting client socket"
        self.client_socket, self.adress = self.listening_socket.accept()
        print(f"Connected to :{self.adress[0]} : {self.adress[1]} ")
        print()

    def serve_request(self):
        "Serve incoming requests from client in a loop until close request is comming"
        request = self.client_socket.recv(self.MESSAGE_BUFFER_SIZE).decode("utf8")
        if request in self.commands:
            match request:
                case "uptime":
                    msg = self.uptime()
                    self.client_socket.send(msg)

                    print("Response send")
                case "info":
                    msg = self.info()
                    self.client_socket.send(msg)
                    print("Response send")
                case "help":
                    msg = self.help()
                    self.client_socket.send(msg)
                    print("Response send")
                case "close":
                    self.close_flag = True
                    self.client_socket.send(messages.SVR_SERVER_CLOSE)
                    self.client_socket.shutdown(s.SHUT_RDWR)
                    self.client_socket.close()
                    print("Closing the server...")
                    sys.exit(0)
                case "new":
                    self.create_user()
                case "delete":
                    self.delete_user()
                case "login":
                    self.login()
                case "messagebox":
                    self.show_messagebox()
                case "message":
                    self.message()
                case "deletemsg":
                    self.deletemsg()
                case "users":
                    self.users()
        
        else:
            self.client_socket.send(
                messages.SVR_SERVER_INVALID_REQUEST
                )
    
    def create_user(self):
        "Serve new user request"                      
        self.client_socket.send(messages.SVR_CREATE_USER_ENTER_USER_NAME)
        name = str(self.client_socket.recv(self.MESSAGE_BUFFER_SIZE).decode("utf8"))
        self.client_socket.send(messages.SVR_CREATE_USER_ENTER_PASSWRD)
        password = str(self.client_socket.recv(self.MESSAGE_BUFFER_SIZE).decode("utf8"))
        result = self.user_administration.create_user(name, password)
        self.client_socket.send(result)

    def delete_user(self):
        "Serve delete user request"
        self.client_socket.send(messages.SVR_DELETE_ENTER_USER_NAME)
        name = self.client_socket.recv(self.MESSAGE_BUFFER_SIZE).decode("utf8")
        result = self.user_administration.delete_user(name)
        self.client_socket.send(result)

    def login(self):
        "Serve user login request"
        self.client_socket.send(messages.SVR_LOGIN_ENTER_USERNAME)
        name = str(self.client_socket.recv(self.MESSAGE_BUFFER_SIZE).decode("utf8"))
        self.client_socket.send(messages.SVR_LOGIN_ENTER_PASSWORD)
        password = str(self.client_socket.recv(self.MESSAGE_BUFFER_SIZE).decode("utf8"))
        result = self.user_administration.log_in(name, password)
        self.client_socket.send(result)
    
    def show_messagebox(self):
        "Serve displaying user messagebox"
        result = self.user_administration.show_messagebox()
        self.client_socket.send(result)

    def message(self):
        "Serve sending message to the user"
        self.client_socket.send(messages.SVR_MESSAGE_ENTER_USERNAME)
        name = str(self.client_socket.recv(self.MESSAGE_BUFFER_SIZE).decode("utf8"))
        self.client_socket.send(messages.SVR_MESSAGE_ENTER_MESSAGE)
        msg = str(self.client_socket.recv(self.MESSAGE_BUFFER_SIZE).decode("utf8"))
        result = self.user_administration.send_message(name, msg)
        self.client_socket.send(result)
       
    def deletemsg(self):
        "Serve deleting user's message"
        self.client_socket.send(messages.SVR_DELETEMSG_MESSAGE_NUM)
        number = str(self.client_socket.recv(self.MESSAGE_BUFFER_SIZE).decode("utf8"))
        result = self.user_administration.delete_message(number)
        self.client_socket.send(result)

    def users(self):
        "Return users list"
        result = self.user_administration.get_users_list()
        self.client_socket.send(result)


class UserAdministration:

    def __init__(self, database):
        self.currently_logged_user = None
        self.db = database
        self.permissions = ["admin", "user"]
        self.not_permitted_username_signs = ["'", "'", "<", ">", "[", "{", "}", "]", ",", "."]
        self.MESSAGE_MAX_LENGHT = 256

    def create_user(self, username, password):
        "Create new user if user not exists and username is correct"
        if self.check_if_username_exist(username):
            return messages.USR_ADMIN_CREATE_USER_NAME_ALREADY_EXIST
        elif not self.check_username_correctness(username):
            return messages.USR_ADMIN_CREATE_USER_INVALID_SIGNS
        else:
            if username == self.permissions[0]:
                role = self.permissions[0]
            else:
                role = self.permissions[1]
            result = self.db.add_user_to_users_table(username, password, role)
            return result
           
    def delete_user(self, username):
        "If logged as admin and user exists - delete user"
        user_permissions = self.get_permissions(self.currently_logged_user)
        if user_permissions != self.permissions[0]:
            return messages.USR_ADMIN_DELETE_NO_PERMISSION
        elif not self.check_if_username_exist(username):
            return messages.USR_ADMIN_DELETE_USER_NOT_EXIST
        else:
            result = self.db.delete_user_from_users_table(username)
            return result
            
    def log_in(self, username, password):
        "If username and password is correct,log user in."
        print("Function:log in:", username)
        check_result = self.db.get_username_and_password(username, password)
        if check_result:
            self.currently_logged_user = username
            return messages.USR_ADMIN_LOGIN_LOGGED_IN
        return messages.USR_ADMIN_LOGIN_INVALID
          
    def get_permissions(self, username):
        "Get user permissions"
        print("Function:get permissions:", username)
        return self.db.get_user_role(username)
                
    def check_username_correctness(self, username):
        "Check if username does not include invalid signs"
        print("Function:Check username correctness:", username)
        for sign in username:
            if sign in self.not_permitted_username_signs:
                return False
        return True
    
    def check_if_username_exist(self, username):
        "Check if username already exists"
        print("Function:Check if username exist:", username)
        result = self.db.check_if_user_exist(username)
        return result
        
    def get_users_list(self):
        "If list is not empty,return list of currently registered users"
        print("Function:Get users list")
        result = self.db.get_users_list()
        if result is None:
            return messages.USR_ADMIN_GET_USR_LIST_NO_REGISTERED_USERS
        else:
            return json.dumps(result, ensure_ascii=False).encode("utf8")
        
    def show_messagebox(self):
        "If user is logged in and user's messagebox is not empty -show user messagebox"
        username = self.currently_logged_user
        print("Function:show messagebox:", username)
        if self.currently_logged_user is None:
            return messages.USR_ADMIN_SHOW_MSGBOX_LOGIN
        else:
            result = self.db.show_messagebox(username)
            if result is None:
                return messages.USR_ADMIN_SHOW_MSGBOX_IS_EMPTY
            else:
                return json.dumps(result, ensure_ascii=False).encode("utf8")
        
    def send_message(self, receiver, message):
        "If sender is logged in and receiver exists and message is not too long - send message"
        print("Function:send message:", receiver)
        sender = self.currently_logged_user
        if not self.check_if_username_exist(receiver):
            return messages.USR_ADMIN_MESSAGE_INVALID_USERNAME
        elif sender is None:
            return messages.USR_ADMIN_SND_MSG_LOGIN_BEFORE_SND
        elif len(message) > self.MESSAGE_MAX_LENGHT:
            return messages.USR_ADMIN_MESSAGE_TOO_LONG
        else:
            result = self.db.send_message(receiver, sender, message)
            return result

    def delete_message(self, number):
        "If user is logged in - delete message by message number"
        username = self.currently_logged_user
        if self.currently_logged_user is None:
            return messages.USR_ADMIN_DEL_MSG_LOGIN_BEFORE_DEL
        else:
            return self.db.delete_message(username, number)


class Client_Server_DB:

    def __init__(self, dbfile=r"Db_client_server"):
        self.dbfile = dbfile
        self.connection = None
        self.cursor = None
        self.MAX_MESSAGEBOX_LENGHT = 5
        self.create_connection()

    def create_connection(self):
        "Creating a database connection for SQLite"
        try:
            self.connection = sqlite3.connect(database=self.dbfile)
            self.get_cursor()
        except Error as e:
            print(e)
            return e
    
    def get_cursor(self):
        "Get connection cursor"
        self.cursor = self.connection.cursor()

    def create_users_table(self):
        "Create table of users"
        query = """ CREATE TABLE IF NOT EXISTS users (
                                        username VARCHAR(50) PRIMARY KEY,
                                        password VARCHAR(50) NOT NULL,
                                        role VARCHAR(6)
                                    ); """
        self.execute_query_without_return_data(query)

    def create_messages_table(self):
        "Create table of messages"
        query = """ CREATE TABLE IF NOT EXISTS messages (
                                        id INTEGER PRIMARY KEY,
                                        username VARCHAR(50) NOT NULL,
                                        sender VARCHAR(50) NOT NULL,
                                        message VARCHAR(255),
                                        FOREIGN KEY (username)
                                            REFERENCES users (username)

                                    ); """
        self.execute_query_without_return_data(query)

    def add_user_to_users_table(self, username, password, role):
        query = " INSERT INTO users(username,password,role) VALUES (?,?,?);"
        query_params = (username, password, role)
        result = self.execute_query_without_return_data(query, query_params)
        if result:
            return messages.DB_ADD_USER_ADDED
        else:
            return messages.DB_ADD_USER_ERROR
            
    def delete_user_from_users_table(self, username):
        query = "DELETE FROM users WHERE username = ?;"
        query_params = (username,)
        result = self.execute_query_without_return_data(query, query_params)
        if result:
            return messages.DB_DELETE_USER_DELETED   
        else:
            return messages.DB_DELETE_USER_ERROR
            
    def send_message(self, receiver, sender, message):
        "If messagebox is not full,send message."
        messagebox_length = self.get_messagebox_length(receiver)
        if messagebox_length < self.MAX_MESSAGEBOX_LENGHT:
            query = "INSERT INTO messages (username,sender,message) VALUES (?,?,?);"
            query_params = (receiver, sender, message)
            result = self.execute_query_without_return_data(query, query_params)
            if result:
                return messages.DB_SEND_MSG_SEND
            else:
                return messages.DB_SEND_MSG_ERROR
        else:
            return messages.DB_SEND_MSG_FULL
    
    def delete_message(self, username, number):
        "if input is number, delete message from messages"
        if number.isdigit():
            query = "DELETE FROM messages WHERE id = ? and username = ?;"
            query_params = (number, username)
            result = self.execute_query_without_return_data(query, query_params)
            if result:
                return messages.DB_DELETE_MSG_DELETED
            else:
                return messages.DB_DELETE_MSG_ERROR
        else:
            return messages.DB_DELETE_MSG_INVALID_SIGNS
        
    def show_messagebox(self, username):
        query = "Select id,sender,message from messages where username = ?;"
        query_params = (username,)
        user_messagebox = self.execute_query_with_return_data(query, query_params)
        if user_messagebox is not None and len(user_messagebox) != 0:
            return user_messagebox
        else:
            return None

    def get_username_and_password(self, username, password):
        query = "SELECT username,password FROM users WHERE username = ? AND password = ?;"
        query_params = (username, password)
        result = self.execute_query_with_return_data(query, query_params)
        if result:
            return True
        else:
            return False
        
    def check_if_user_exist(self, username):
        query = "SELECT EXISTS(SELECT 1 FROM users WHERE username = ?);"
        query_params = (username,)
        result = self.execute_query_with_return_data(query, query_params)
        if result is not None and int(result[0][0]) == 0:
            return False
        else:
            return True
        
    def get_users_list(self):
        query = "SELECT username,role FROM users;"
        result = self.execute_query_with_return_data(query)
        if len(result) != 0:
            return result
        else:
            return None
        
    def get_user_role(self, username):
        query = "SELECT role from users where username = ?;"
        query_params = (username,)
        result = self.execute_query_with_return_data(query, query_params)
        if result:
            return result[0][0]
        else:
            return messages.DB_GET_USR_ROLE_ERROR
        
    def get_messagebox_length(self, username):
        query = "SELECT COUNT(*) FROM messages where username = ?;"
        query_params = (username,)
        result = self.execute_query_with_return_data(query, query_params)
        return int(result)[0][0]

    def execute_query_with_return_data(self, query, data=tuple()):
        "Execute queries which return data"
        try:
            self.cursor.execute(query, data)
            result = self.cursor.fetchall()
            return result
        except Error as e:
            print(e)
            return None

    def execute_query_without_return_data(self, query, data=tuple()):
        "Execute queries which don't return data"
        try:
            self.cursor.execute(query, data)
            self.connection.commit()
            return True
        except Error as e:
            self.connection.rollback()
            print(e)
            return False
