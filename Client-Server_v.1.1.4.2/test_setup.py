from serverlib import Client_Server_DB


def test_database_setup():
    database = Client_Server_DB(r"Test_DB")
    database.create_users_table()
    database.create_messages_table()
