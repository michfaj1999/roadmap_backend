import unittest
from serverlib import Client_Server_DB
from test_setup import test_database_setup
from sqlite3 import Error

test_database_setup()


class TestDataBase(unittest.TestCase):

    def prepare_test_case(self):
        "Preparing data for tests"
        self.testcase_queries = [
            ("INSERT INTO users(username,password,role) VALUES (?,?,?);", ("test_user_1", "password", "user")),
            ("INSERT INTO users(username,password,role) VALUES (?,?,?);", ("test_user_2", "password", "user")),
            ("INSERT INTO users(username,password,role) VALUES (?,?,?);", ("admin", "password", "admin")),
            ("INSERT INTO messages(username,sender,message) VALUES (?,?,?);", ("admin", "test_user_1", "delete test message")),
            ("INSERT INTO messages(username,sender,message) VALUES (?,?,?);", ("test_user_1", "admin", "show_messagebox test message 1")),
            ("INSERT INTO messages(username,sender,message) VALUES (?,?,?);", ("test_user_1", "admin", "show_messagebox test message 2")),
            ("INSERT INTO messages(username,sender,message) VALUES (?,?,?);", ("test_user_2", "admin", "test_send_message_length message 1")),
            ("INSERT INTO messages(username,sender,message) VALUES (?,?,?);", ("test_user_2", "admin", "test_send_message_length message 2")),
            ("INSERT INTO messages(username,sender,message) VALUES (?,?,?);", ("test_user_2", "admin", "test_send_message_length message 3")),
            ("INSERT INTO messages(username,sender,message) VALUES (?,?,?);", ("test_user_2", "admin", "test_send_message_length message 4")),
            ("INSERT INTO messages(username,sender,message) VALUES (?,?,?);", ("test_user_2", "admin", "test_send_message_length message 5"))


        ]
        for test_query in self.testcase_queries:
            query = test_query[0]
            query_params = test_query[1]
            try:
                self.database.cursor.execute(query, query_params)
                self.database.connection.commit()
            except Error as e:
                self.database.connection.rollback()
                print(e)

    def clear_data_after_test(self):
        "Deleting all record from tables"
        query = "DELETE FROM users;"
        self.database.cursor.execute(query)
        self.database.connection.commit()
        query = "DELETE FROM messages;"
        self.database.cursor.execute(query)
        self.database.connection.commit()
    
    def check_if_record_exists_in_users_table(self, username):
        check_query = "SELECT COUNT(*) FROM users WHERE username = ?;"
        check_query_params = (username,)
        self.database.cursor.execute(check_query, check_query_params)
        result = int(self.database.cursor.fetchone()[0])
        if result == 1:
            return True
        elif result is None or result != 1:
            return False
        
    def check_if_record_exists_in_messages_table(self, receiver, sender, message):
        check_query = "SELECT COUNT(*) FROM messages WHERE username = ? and sender = ? and message = ?;"
        check_query_data = (receiver, sender, message)
        self.database.cursor.execute(check_query, check_query_data)
        result = int(self.database.cursor.fetchone()[0]) 
        if result == 1:
            return True
        elif result is None or result != 1:
            return False

    def setUp(self):
        self.database = Client_Server_DB(r"Test_DB")
        self.prepare_test_case()
        self.username = None
        self.password = None
        self.role = None
        self.receiver = None

    def tearDown(self):
        self.clear_data_after_test()

    def test_add_user_to_users_list(self):
        "Check if user was added to users table"
        self.username = "add_user_test"
        self.password = "password"
        self.role = "user"
        self.database.add_user_to_users_table(self.username, self.password, self.role)
        self.assertIs(self.check_if_record_exists_in_users_table(self.username), True)

    def test_delete_user_from_users_list(self):
        "Check if user was deleted from users table"
        self.username = "test_user_1"
        self.database.delete_user_from_users_table(self.username)
        self.assertIs(self.check_if_record_exists_in_users_table(self.username), False)

    def test_send_message(self):
        "Check if message was sent to user"
        self.username = "admin"
        self.receiver = "test_user_1"
        self.message = "Test message"
        self.database.send_message(self.username, self.receiver, self.message)
        self.assertIs(self.check_if_record_exists_in_messages_table(self.username, self.receiver, self.message), True)
    
    def test_send_message_to_full_messagebox(self):
        "Returns information about full messagebox"
        self.username = "test_user_2"
        self.receiver = "admin"
        self.message = "full messagebox test message"
        actual = self.database.send_message(self.username, self.receiver, self.message)
        expected = b"messagebox is full"
        self.assertEqual(actual, expected)
       
    def test_delete_message(self):
        "Check if message was deleted from messages"
        self.username = "admin"
        self.message_number_to_delete = "1"
        self.receiver = "admin"
        self.sender = "test_user_1"
        self.message = "delete test message"
        self.database.delete_message(self.username, self.message_number_to_delete)
        self.assertIs(
            self.check_if_record_exists_in_messages_table(self.receiver, self.sender, self.message), False
            )
    
    def test_delete_message_wrong_input(self):
        "Returns information for wrong input format"
        self.username = "admin"
        self.message_number_to_delete = "A"
        actual = self.database.delete_message(self.username, self.message_number_to_delete)
        expected = b"only numbers allowed"
        self.assertEqual(actual, expected)

    def test_show_messagebox(self):
        "Show user's messagebox"
        self.username = "test_user_1"
        actual = self.database.show_messagebox(self.username)
        expected = [(2, 'admin', 'show_messagebox test message 1'),
                  (3, 'admin', 'show_messagebox test message 2'),]
        self.assertEqual(data, result)

    def test_show_messagebox_not_existing_user(self):
        "Returns None for not existing user"
        self.username = "unknown_user"
        result = self.database.show_messagebox(self.username)
        self.assertIsNone(result)

    def test_get_user_role(self):
        "Returns role user for normal user"
        self.username = "test_user_2"
        data = self.database.get_user_role(self.username)
        result = "user"
        self.assertEqual(data, result)

    def test_get_username_and_password_correct(self):
        "Returns True for correct username and password"
        self.username = "test_user_2"
        self.password = "password"
        result = self.database.get_username_and_password(self.username, self.password)
        self.assertIs(result, True)

    def test_get_username_and_password_wrong(self):
        "Returns False for wrong username and password"
        self.username = "wrong_username"
        self.password = "wrong_password"
        result = self.database.get_username_and_password(self.username, self.password)
        self.assertIs(result, False)

    def test_check_if_user_exists(self):
        "Returns True for existing user"
        self.username = "test_user_2"
        result = self.database.check_if_user_exist(self.username)
        self.assertIs(result, True)
          
    def test_check_if_user_not_exists(self):
        "Returns False for not existing user"
        self.username = "unknown_user"
        result = self.database.check_if_user_exist(self.username)
        self.assertIs(result, False)
