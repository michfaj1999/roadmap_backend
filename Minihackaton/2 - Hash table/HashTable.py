
class HashTable:
    def __init__(self, size=10):
        self.size = size
        self.table = [[] for _ in range(self.size)]
        self.items_count = 0

    def _hash_function(self, key):
        return hash(key)

    def insert(self, key, value):
        "Inserts key-value pair to hashtable. In case of collision uses open adressing and linear search for new place"
        value_tuple = (key, value)
        if self.items_count == self.size:
            raise Exception("Hash table is full")
        else:
            index = self._hash_function(key) % self.size
            if len(self.table[index]) == 0:
                self.table[index].append(value_tuple)
                self.items_count += 1
                return
            else:
                for _ in range(len(self.table)):
                    if len(self.table[index]) > 0 and self.table[index][0][0] == key:
                        raise Exception("key must be unique")
                    if len(self.table[index]) == 0:
                        self.table[index].append(value_tuple)
                        self.items_count += 1
                        return
                    if index == len(self.table)-1:
                        index = 0
                        continue
                    index += 1

    def remove(self, key):
        "If key is in the hash table - removes key-value pair from hashtable.Otherwise raise Exception."
        index = self._hash_function(key) % self.size
        cell = self.table[index]
        if len(cell) == 0 or cell[0][0] != key: #hash is different than calculated or cell is empty
            for _ in range(len(self.table)):
                if len(self.table[index]) > 0 and key in self.table[index][0][0]:
                    self.table[index].pop(0)
                    self.items_count -= 1
                    return
                if index == len(self.table)-1:
                    index = 0
                    continue
                index += 1
        else:
            if len(cell) > 0:
                cell.pop(0)
                self.items_count -= 1
                return 
        raise Exception("Key not found")

    def search(self, key):
        "If key is in the hash table - returns value associated with key.Otherwise - raises Exception"
        index = self._hash_function(key) % self.size
        cell = self.table[index]
        if len(cell) > 0 and key in cell[0]:
            return cell[0][1]
        else: #if hash is different than calculated
            for _ in range(len(self.table)):
                if len(self.table[index]) > 0 and key in self.table[index][0]:
                    return self.table[index][0][1]
                if index == len(self.table)-1:
                    index = 0
                    continue
                index += 1
        raise Exception("Key not found")

    def contains(self, key):
        "Returns True if passed key is in hashtable,otherwise - returns False"
        index = self._hash_function(key) % self.size
        cell = self.table[index]
        if len(cell) > 0 and key in cell[0]:
            return True
        else:
            for _ in range(len(self.table)):
                if len(self.table[index]) > 0 and key in self.table[index][0]:
                    return True
                if index == len(self.table)-1:
                    index = 0
                    continue
                index += 1

        return False

    def count_elements(self):
        "Returns current count of elements in hashtable"
        return self.items_count


