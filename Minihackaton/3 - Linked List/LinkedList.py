class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self, data_type=int):
        self.data_type = data_type
        self._head = None
        self._last = None
        self._current_node_pointer = None
        self._list_size = 0

    def _is_data_valid(self, data):
        """Checks if specified data has valid type

            Args:
                data - data to check
            Returns:
                True - if data has valid type"""

        if isinstance(data, self.data_type):
            return True

    def _is_data_unique(self, data):
        """Checks if specified data is not currently on the list

            Args:
                data - data to check
            Returns:
                True - if data is unique"""
        if self.search(data) is None:
            return True
        
    def _move_pointer_to_the_next_node(self):
        """Moves pointer to the next node"""
        self._current_node_pointer = self._current_node_pointer.next

    def insert_at_beginning(self, data):
        """Insert new Node at the beginning of the list 
            Args:
                data - data to insert
            Raises:
                Exception - If data type is invalid or if data is not unique or
                key is out of list range"""
        
        if not self._is_data_valid(data):
            raise Exception("Insertion error - invalid data type")
        if not self._is_data_unique(data):
            raise Exception("Inserted data must be Unique")

        if self._head is None:
            self._head = Node(data)
            self._last = self._head 
            self._current_node_pointer = self._head
            self._list_size += 1
            return
        else:
            temp_pointer = self._head
            self._head = Node(data)
            self._head.next = temp_pointer
            self._current_node_pointer = self._head
            if self._current_node_pointer.next.next is None:
                self._last = self._current_node_pointer.next
            self._list_size += 1
            return

    def insert_at_end(self, data):
        """Insert new Node at the end of the list. 
            Args:
                data - data to insert
            Raises:
                Exception - If data type is invalid or if data is not unique"""
        if not self._is_data_valid(data):
            raise Exception("Insertion error - invalid data type")
        if not self._is_data_unique(data):
            raise Exception("Inserted data must be Unique")
        
        if self._head is None:
            self._head = Node(data)
            self._last = self._head
            self._current_node_pointer = self._head
            self._list_size += 1
            return
        else:
            new_node = Node(data)
            self._last.next = new_node
            self._last = new_node
            self._current_node_pointer = self._head
            self._list_size += 1
        
    def insert_at_position(self, data, key):
        """Insert new Node after position specified by key. 
            Args:
                data - data to insert
                key - position after data should be inserted
            Raises:
                Exception - If data type is invalid or if data is not unique or
                key is out of list range"""

        if not self._is_data_valid(data):
            raise Exception("Insertion error - invalid data type")
        if not self._is_data_unique(data):
            raise Exception("Inserted data must be Unique")
        if key == 0:
            self.insert_at_beginning(data)
            return
        if key > self._list_size:
            raise Exception("Insertion key out of range")
        count = 1
        while count != key and self._current_node_pointer.next is not None:
            self._move_pointer_to_the_next_node()
            count +=1
        elem_to_insert = Node(data)
        temp_pointer = self._current_node_pointer.next
        self._current_node_pointer.next = elem_to_insert
        if self._current_node_pointer.next is None:
            self._last = elem_to_insert
        elem_to_insert.next = temp_pointer
        self._current_node_pointer = self._head
        self._list_size += 1
        
    def delete_from_beginning(self):
        """Delete Node from the beginning of the list"""
        if self._head is None:
            return
        if self._head.next is None:
            del self._head
            self._head = None
            self._last = None
            self._current_node_pointer = None
            self._list_size -= 1
            return
        temp_pointer = self._head.next
        del self._head
        self._head = temp_pointer
        self._current_node_pointer = self._head
        self._list_size -= 1

    def delete_from_end(self):
        """Delete Node from the end of the list"""
        if self._head is None:
            return
        if self._head.next is None:
            del self._head
            self._head = None
            self._last = None
            self._current_node_pointer = None
            self._list_size -= 1
            return
        while self._current_node_pointer.next != self._last:
            self._move_pointer_to_the_next_node()

        del self._last
        self._last = self._current_node_pointer
        self._last.next = None
        self._current_node_pointer = self._head
        self._list_size -= 1

    def delete_node(self, key):
        """Delete Node specified by data key
            Args:
                key - data to delete"""

        if self._head is None:
            return
    
        if self._head.data == key:
            temp_next = self._head.next
            del self._head
            self._head = temp_next
            self._current_node_pointer = self._head
            self._list_size -= 1
            return
    
        while self._current_node_pointer.next is not None:
            if self._current_node_pointer.next.data == key:
                if self._current_node_pointer.next.data == self._last.data:
                    self._last = self._current_node_pointer
                temp_next = self._current_node_pointer.next.next
                del self._current_node_pointer.next
                self._current_node_pointer.next = temp_next
                self._current_node_pointer = self._head
                self._list_size -= 1
                return
            self._move_pointer_to_the_next_node()
        self._current_node_pointer = self._head

    def search(self, key):
        """Find and return the node specified by data key or return None if key is not found
            Args:
                key - data to find
            Returns:
                Node() - reference to Node with specified data
                None - if key is not found"""
        if self._head is None:
            return None
        while self._current_node_pointer is not None:
            if self._current_node_pointer.data == key:
                node_to_return = self._current_node_pointer
                self._current_node_pointer = self._head
                return node_to_return
            self._move_pointer_to_the_next_node()
        self._current_node_pointer = self._head
        return None
        
    def display(self):
        """Display all elements from the list.If the list is empty - print Empty"""
        if self._head is None:
            return print("Empty")
        while self._current_node_pointer is not None:
            print(self._current_node_pointer.data)
            self._move_pointer_to_the_next_node()
        self._current_node_pointer = self._head

    def clear(self):
        """Delete all node from from the list"""
        if self._head is None:
            return
        while self._current_node_pointer is not None:
            temp_pointer = self._current_node_pointer
            self._move_pointer_to_the_next_node()
            del temp_pointer
        self._list_size = 0
        self._current_node_pointer = None
        self._head = None
        self._last = None
        return
            
    def size(self):
        """Return current size of the list"""
        return self._list_size
        


# Użycie klasy LinkedList
ll = LinkedList()
ll.insert_at_beginning(10)
ll.insert_at_end(20)
ll.insert_at_position(15, 1)  # Wstaw 15 za elementem 1
ll.insert_at_end(23)
ll.insert_at_position(17, 4)
ll.insert_at_position(13, 0)
ll.display()
print("Dotad")
ll.delete_from_beginning()
ll.display()
print("dotad")
ll.delete_node(15)
ll.display()
ll.clear()
print("Rozmiar listy po wyczyszczeniu:", ll.size())
ll.display()
