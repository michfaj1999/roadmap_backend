
"""Task: Prepare your own implementation of Priority Queue.
Queue should have following fetures:
- add item at the end of the queue
- get and delete first item from queue
- check first item in the queue (without getting it)
- check if queue is empty
- get queue lenght
"""

class PriorityQueue:

    def __init__(self, priority_range=1):
        
        self.queue = {}
        self._queue_length = 0
        for i in sorted([i+1 for i in range(priority_range)], reverse=True):
            self.queue[i] = []

    def add(self, element, priority):
        "add new element to the queue"
        self.queue[priority].append(element)
        self._queue_length += 1

    def get_item(self):
        "get first element from the queue"
        for key in range(max(self.queue.keys()), 0, -1):
            if len(self.queue[key]) != 0:
                self._queue_length -= 1
                return self.queue[key].pop(0)
        return IndexError("Queue is empty")
    
    def show_first_item(self):
        "show first element without getting it"
        for key in range(max(self.queue.keys()), 0, -1):
            if len(self.queue[key]) != 0:
                return self.queue[key][0]       
        return IndexError("Queue is empty")
       
    def __len__(self):
        "get no of elements in queue"
        return self._queue_length
           
    def is_empty(self):
        "check if queue is empty"
        return self._queue_length == 0
        