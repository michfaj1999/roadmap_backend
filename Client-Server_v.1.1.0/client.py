import socket as s
import sys

HOST = "127.0.0.1"
PORT = 33000
BUFFER = 1024


while True:
    client_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
    client_socket.connect((HOST, PORT))
    request = input("Enter request for a server:").encode("utf8")
    print()

    client_socket.send(request)
    msg = client_socket.recv(BUFFER).decode("utf8")
    if msg == "close":
        print("Closing server and client")
        sys.exit(0)
    print(msg)
    print()
