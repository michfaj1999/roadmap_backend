import socket as s
import json
from datetime import datetime
import time
import sys


def uptime():
    live_time_stop = time.time()
    live_time = live_time_stop - live_time_start

    dictionary = {
        "time": str(round(live_time, 2)),
    }
    return json.dumps(dictionary, ensure_ascii=False).encode("utf8")


def info():
    dictionary = {
        "server_version": server_version,
        "start_date": str(formated_time),
    }
    return json.dumps(dictionary, ensure_ascii=False).encode("utf8")


def help():
    dictionary = {
        "uptime": "returns server livetime",
        "info": "return server version and start date",
        "close": "close connection with server"
    }
    return json.dumps(dictionary, ensure_ascii=False).encode("utf8")


HOST = "127.0.0.1"
PORT = 33000
BUFFER = 1024

commands = ("uptime", "info", "help", "close")
server_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen()
live_time_start = time.time()
start_time = datetime.now()
formated_time = start_time.strftime("%d/%m/%Y %H:%M:%S")
server_version = "1.1.0"
close_flag = False

print("Server starting...")
print()

while not close_flag:
    client_socket, adress = server_socket.accept()
    print(f"Connected to :{adress[0]} : {adress[1]} ")
    print()
    request = client_socket.recv(BUFFER).decode("utf8")
    if not request:
        break
    print(f"Incoming request from client:{request}")
    if request in commands:
        match request:
            case "uptime":
                msg = uptime()
                client_socket.send(msg)
                client_socket.close()
                print("Response send")
            case "info":
                msg = info()
                client_socket.send(msg)
                client_socket.close()
                print("Response send")
            case "help":
                msg = help()
                client_socket.send(msg)
                client_socket.close()
                print("Response send")
            case "close":
                client_socket.send(b"close")
                client_socket.shutdown(s.SHUT_RDWR)
                client_socket.close()
                print("Closing the server...")
                sys.exit(0)                        
    else:
        client_socket.send(b"invalid request.type help to see commands")
        client_socket.close()
