import time
from datetime import datetime
import socket as s
import sys
import json
import messages

class Server:

    def __init__(self, HOST, PORT, BUFFER, UserAdministration, MessageBox):
        self.HOST = HOST
        self.PORT = PORT
        self.BUFFER = BUFFER
        self.lifetime_start = time.time()
        self.start_date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        self.server_version = "v.1.1.2"
        self.close_flag = False
        self.commands = ("uptime", "info", "help", "close", "new", "delete",
                         "login", "messagebox", "message", "deletemsg")
        self.user_administration = UserAdministration
        self.messagebox = MessageBox

    def uptime(self):
        live_time_stop = time.time()
        live_time = live_time_stop - self.lifetime_start

        uptime = {
            "time": str(round(live_time, 2)),
        }
        return json.dumps(uptime, ensure_ascii=False).encode("utf8")

    def help(self):
        commands = {
            "uptime": "returns server lifetime",
            "info": "return server version and start date",
            "close": "close connection with server",
            "new": "adding new user",
            "login": "log in to your user account",
            "messagebox": "show messagebox",
            "message": "send message to another user",
            "deletemsg": "delete message from you messagebox"
        }
        return json.dumps(commands, ensure_ascii=False).encode("utf8")

    def info(self):
        server_info = {
            "server_version": self.server_version,
            "start_date": str(self.start_date),
        }
        return json.dumps(server_info, ensure_ascii=False).encode("utf8")

    def set_listening_socket(self):
        print("Server starting...")
        print()
        self.listening_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.listening_socket.bind((self.HOST, self.PORT))
        self.listening_socket.listen()

    def accept_client_socket(self):
        self.client_socket, self.adress = self.listening_socket.accept()
        print(f"Connected to :{self.adress[0]} : {self.adress[1]} ")
        print()

    def serve_request(self):
        request = self.client_socket.recv(self.BUFFER).decode("utf8")
        if request in self.commands:
            match request:
                case "uptime":
                    msg = self.uptime()
                    self.client_socket.send(msg)
                    print("Response send")
                case "info":
                    msg = self.info()
                    self.client_socket.send(msg)
                    print("Response send")
                case "help":
                    msg = self.help()
                    self.client_socket.send(msg)
                    print("Response send")
                case "close":
                    self.client_socket.send(b"close")
                    self.client_socket.shutdown(s.SHUT_RDWR)
                    self.client_socket.close()
                    print("Closing the server...")
                    sys.exit(0)
                case "new":
                    self.create_user()
                case "delete":
                    self.delete_user()
                case "login":
                    self.login()
                case "messagebox":
                    self.show_messagebox()
                case "message":
                    self.message()
                case "deletemsg":
                    self.deletemsg()

        else:
            self.client_socket.send(
                messages.SERVER_INVALID_REQUEST
                )

    def create_user(self):
        self.client_socket.send(messages.SERVER_CREATE_USER_ENTER_USERNAME)
        name = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        self.client_socket.send(messages.SERVER_CREATE_USER_ENTER_PASSWORD)
        password = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        result = self.user_administration.create_user(name, password)
        self.client_socket.send(result)

    def delete_user(self):
        self.client_socket.send(messages.SERVER_DELETE_USER_ENTER_USERNAME)
        name = self.client_socket.recv(self.BUFFER).decode("utf8")
        result = self.user_administration.delete_user(name)
        self.client_socket.send(result)

    def login(self):
        self.client_socket.send(messages.SERVER_LOGIN_ENTER_USERNAME)
        name = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        self.client_socket.send(messages.SERVER_LOGIN_ENTER_PASSWORD)
        password = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        result = self.user_administration.log_in(name, password)
        self.client_socket.send(result)
    
    def show_messagebox(self):
        result = self.user_administration.show_messagebox()
        self.client_socket.send(result)
       
    def message(self):
        self.client_socket.send(messages.SERVER_MESSAGE_ENTER_RECEIVER)
        name = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        self.client_socket.send(messages.SERVER_MESSAGE_ENTER_MESSAGE)
        msg = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        result = self.user_administration.send_message(name, msg)   
        self.client_socket.send(result)

    def deletemsg(self):
        self.client_socket.send(messages.SERVER_DELETE_MESSAGE_ENTER_NUMBER)
        number = int(self.client_socket.recv(self.BUFFER).decode("utf8"))
        result = self.user_administration.delete_message(number)
        self.client_socket.send(result)


class UserAdministration:

    def __init__(self, messagebox, JsonWriter=None, Saveing=False):
        self.UserList = []
        self.currently_logged_user = None
        self.messagebox = messagebox
        self.permissions = ("admin", "user")
        self.not_permitted_username_signs = ("'", "'", "<", ">", "[", "{", "}", "]", ",", ".")
        self.JsonWriter = JsonWriter
        self.Saveing = Saveing
        self.read_data_from_json()

    def create_user(self, username, password):
        if not self.check_username_correctness(username):
            return messages.USER_ADMIN_CREATE_USER_INVALID_SIGNS
        else:
            role = self.permissions[0] if username == self.permissions[0] else self.permissions[1]
            self.UserList.append((username, password, role))
            self.messagebox.create_messagebox(username)
            self.save_data_to_json()
            return messages.USER_ADMIN_CREATE_USER_COMPLETED
            
    def delete_user(self, username):
        permissions = self.get_permissions(self.currently_logged_user)
        if permissions != self.permissions[0]:
            return messages.USER_ADMIN_DELETE_USER_NO_PERMISSION
        elif not self.check_if_username_exist(username):
            return messages.USER_ADMIN_DELETE_USER_NOT_EXISTS
        else:
            for record in self.UserList:
                if username in record:
                    self.UserList.remove(record)
            self.messagebox.delete_messagebox(username)
            self.save_data_to_json()
            return messages.USER_ADMIN_DELETE_USER_COMPLETED
            
    def log_in(self, username, password):
        if self.check_username_and_password(username, password):
            self.currently_logged_user = username
            return messages.USER_ADMIN_LOGIN_COMPLETED
        else:
            return messages.USER_ADMIN_LOGIN_INVALID
    
    def show_messagebox(self):
        if self.currently_logged_user is None:
            return messages.USER_ADMIN_SHOW_MESSAGEBOX_LOGIN
        else:
            return self.messagebox.show_messagebox(self.currently_logged_user)
        
    def send_message(self, receiver, message):
        sender = self.currently_logged_user
        return self.messagebox.send_message(sender, receiver, message)
        
    def delete_message(self, number):
        username = self.currently_logged_user
        return self.messagebox.delete_message(username, number)
    
    def check_username_and_password(self, username, password):
        for record in self.UserList:
            if record[0] == username and record[1] == password:
                return True
        return False
    
    def get_permissions(self, username):
        for record in self.UserList:
            if record[0] == username:
                return record[2]
                
    def check_username_correctness(self, username):
        for sign in username:
            if sign in self.not_permitted_username_signs:
                return False
        return True
    
    def check_if_username_exist(self, username):
        for record in self.UserList:
            if record[0] == username:
                return True
        return False
    
    def read_data_from_json(self):
        if self.Saveing:
            self.UserList = self.JsonWriter.read_from_file()
            if self.UserList is None:
                self.UserList = []
    
    def save_data_to_json(self):
        if self.Saveing:
            self.JsonWriter.write_to_file(self.UserList)


class MessageBox:
    def __init__(self, JsonWriter=None, Saveing=False):
        self.messageBoxes = dict()
        self.messagebox_field_numbers = range(1, 6)
        self.MESSAGE_MAX_LENGHT = 256
        self.placeholder = "empty"
        self.JsonWriter = JsonWriter
        self.messagebox_template = {
                "1": self.placeholder,
                "2": self.placeholder,
                "3": self.placeholder,
                "4": self.placeholder,
                "5": self.placeholder,
            } 
        self.Saveing = Saveing
        self.read_data_from_json()
        
    def create_messagebox(self, username):
        self.messageBoxes[username] = self.messagebox_template
        self.save_data_to_json()

    def delete_messagebox(self, username):
        del self.messageBoxes[username]
        self.save_data_to_json()

    def show_messagebox(self, username):
        if username in self.messageBoxes.keys():   
            return json.dumps(self.messageBoxes[username], ensure_ascii=False).encode("utf8")
        else:
            return messages.MESSAGEBOX_SHOW_MESSAGEBOX_NOT_FOUND
        
    def send_message(self, sender, receiver, message):
        if not self.check_if_messagebox_exist(receiver):
            return messages.MESSAGEBOX_SEND_MESSAGE_INVALID_RECEIVER
        elif sender is None:
            return messages.MESSAGEBOX_SEND_MESSAGE_LOGIN
        elif self.check_if_messagebox_is_full(receiver):
            return messages.MESSAGEBOX_SEND_MESSAGE_IS_FULL
        elif len(message) > self.MESSAGE_MAX_LENGHT:
            return messages.MESSAGEBOX_SEND_MESSAGE_TOO_LONG
        else:
            for message_number, message_field in self.messageBoxes[receiver].items():
                if message_field == self.placeholder:
                    self.messageBoxes[receiver][message_number] = [sender, message]
                    self.save_data_to_json()
                    return messages.MESSAGEBOX_SEND_MESSAGE_COMPLETED
       
    def check_if_messagebox_is_full(self, username):
        if self.placeholder not in self.messageBoxes[username].values():
            return False
        else:
            return True
        
    def delete_message(self, username, number):
        if number not in self.messagebox_field_numbers:
            return messages.MESSAGEBOX_DELETE_MESSAGE_NUMBER_OUT_OF_RANGE
        else:
            self.messageBoxes[username][number] = self.placeholder
            self.save_data_to_json()
            return messages.MESSAGEBOX_DELETE_MESSAGE_COMPLETED
  
    def check_if_messagebox_exist(self, username):
        if username in self.messageBoxes.keys():
            return True
        else:
            return False
        
    def read_data_from_json(self):
        if self.Saveing:
            self.messageBoxes = self.JsonWriter.read_from_file()
            if self.messageBoxes is None:
                self.messageBoxes = dict()
            
    def save_data_to_json(self):
        if self.Saveing:
            self.JsonWriter.write_to_file(self.messageBoxes)


class JsonWriter:
    def __init__(self, file_path):
        self.file_path = file_path

    def write_to_file(self, data_to_write):
        try:
            with open(self.file_path, 'w+') as file:
                json.dump(data_to_write, file, indent=4)
                return True
        except Exception as e:
            print(f"Error saving data to file '{self.file_path}': {e}")
            return False
            
    def read_from_file(self):
        try:
            with open(self.file_path, 'r') as file:
                data = json.load(file)
                return data
        except FileNotFoundError:
            print(f"File '{self.file_path}' not found.")
            return None
        except json.JSONDecodeError as e:
            print(f"Error decoding JSON in file '{self.file_path}': {e}")
            return None
            