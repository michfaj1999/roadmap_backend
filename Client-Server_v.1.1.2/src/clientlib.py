import socket as s
import sys


class Client:

    def __init__(self, HOST, PORT, BUFFER):
        self.HOST = HOST
        self.PORT = PORT
        self.BUFFER = BUFFER
    
    def set_connection(self):
        self.client_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.client_socket.connect((self.HOST, self.PORT))
    
    def send_request(self):
        request = input("Enter request for a server:").encode("utf8")
        print()
        if request:
            self.client_socket.send(request)
            msg = self.client_socket.recv(self.BUFFER).decode("utf8")
            if msg == "close":
                print("Closing server and client")
                sys.exit(0)
            print(msg)
            print()
        else:
            print("You cannot send empty request.Enter value")
