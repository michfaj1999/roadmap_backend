import serverlib

HOST = '127.0.0.1'
PORT = 33000
BUFFER = 1024

messagebox_jsonwriter = serverlib.JsonWriter("messageboxes.json")
userlist_jsonwriter = serverlib.JsonWriter("userlist.json")
messagebox = serverlib.MessageBox(JsonWriter=messagebox_jsonwriter, Saveing=False)
user_administration = serverlib.UserAdministration(messagebox, JsonWriter=userlist_jsonwriter, Saveing=False)
server = serverlib.Server(HOST, PORT, BUFFER, user_administration, messagebox)
server.set_listening_socket()
server.accept_client_socket()
while not server.close_flag:
    server.serve_request()