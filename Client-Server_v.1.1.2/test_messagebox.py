import unittest
from src.serverlib import MessageBox


class TestMessageBox(unittest.TestCase):

    def prepare_test_case(self):
        placeholder = self.messagebox.placeholder
        self.messagebox.messageBoxes["test_user_1"] = {
            "1": placeholder,
            "2": placeholder,
            "3": placeholder,
            "4": placeholder,
            "5": placeholder
            }
        self.messagebox.messageBoxes["test_user_2"] = {
            "1": ["test_user_1", "hello"],
            "2": placeholder,
            "3": placeholder,
            "4": placeholder,
            "5": placeholder
            }
        
    def check_if_messagebox_exists(self, username):
        if username in self.messagebox.messageBoxes.keys():
            return True
        else:
            return False
    
    def check_if_message_exists(self, name, message):
        message_sequence = [name, message]
        for messagebox in self.messagebox.messageBoxes.keys():
            if message_sequence in self.messagebox.messageBoxes[messagebox].values():
                return True
        return False

    def setUp(self):
        self.messagebox = MessageBox(Saveing=False)
        self.prepare_test_case()
    
    def test_create_admin_messagebox(self):
        self.messagebox.create_messagebox("admin")
        self.assertEqual(self.check_if_messagebox_exists("admin"), True)

    def test_create_user_messagebox(self):
        self.messagebox.create_messagebox("create_user_test")
        self.assertEqual(self.check_if_messagebox_exists("create_user_test"), True)

    def test_delete_user_messagebox(self):
        self.messagebox.delete_messagebox("test_user_1")
        self.assertEqual(self.check_if_messagebox_exists("test_user_1"), False)

    def test_send_message(self):
        self.messagebox.send_message("test_user_2", "test_user_1", "hello")
        self.assertEqual(self.check_if_message_exists("test_user_1","hello"), True)

    def test_send_message_without_logging_in(self):
        result = self.messagebox.send_message(None,"test_user_1","You cannot send message without sender field")
        self.assertEqual(result, b"you have to log in before sending message")

    def test_send_message_with_invalid_receiver_name(self):
        result = self.messagebox.send_message("admin", "uzer1", "hello")
        self.assertEqual(result, b"invalid receiver name")
        
    def test_delete_message(self):
        self.messagebox.delete_message("test_user_2", 1)
        self.assertEqual(self.check_if_message_exists("test_user_2", "hello"), False)

    def test_delete_messagebox_with_num_bigger_than_messagebox_size(self):
        result = self.messagebox.delete_message("test_user_1", 7)
        self.assertEqual(result, b"message number not in range of messagebox size")

    def test_show_messagebox_with_invalid_username(self):
        result = self.messagebox.show_messagebox("uzer1")
        self.assertEqual(result, b"messagebox for user not found")

  
