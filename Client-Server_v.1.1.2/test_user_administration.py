from unittest.mock import Mock
import unittest
from src.serverlib import UserAdministration


mock = Mock()
messagebox = mock


class TestUserAdministration(unittest.TestCase):
     
    def prepare_test_case(self):
        test_users = [
            ("test_user_1", "password", self.user_administration.permissions[1]),
            ("test_user_2", "password", self.user_administration.permissions[1]),
        ]
        for record in test_users:
            self.user_administration.UserList.append(record),

    def setUp(self):
        self.user_administration = UserAdministration(messagebox, Saveing=False)
        self.prepare_test_case()
        
    def test_create_user(self):
        "Checks if new user was added to UserList"
        self.user_administration.create_user("test_create_user", "password")
        if ("test_create_user", "password", "user") in self.user_administration.UserList:
            assert True
        else:
            assert False
    
    def test_create_user_as_admin(self):
        "Checks if new user was added to UserList with role admin"
        self.user_administration.create_user("admin", "password1")
        if ("admin", "password1", "admin") in self.user_administration.UserList:
            assert True
        else:
            assert False
    
    def test_create_user_with_not_permitted_signs(self):
        "Returns information about not permitted signs in name"
        self.assertEqual(self.user_administration.create_user(".,/;';", "password"), b"Invalid signs in username")

    def test_login_user(self):
        self.user_administration.log_in("test_user_1", "password")
        self.assertEqual(self.user_administration.currently_logged_user, "test_user_1")

    def test_delete_user(self):
        self.user_administration.delete_user("test_user_1")
        if ("test_user_1", "password", "user") not in self.user_administration.UserList:
            assert True
        else:
            assert False

    def test_delete_user_log_in_as_admin(self):
        result = self.user_administration.delete_user("uzer1")
        self.assertEqual(result, b"not permitted to delete users.Login as admin")
        
    def test_get_permission(self):
        result = self.user_administration.get_permissions("test_user_1")
        self.assertEqual(result, "user")
    
    def test_check_if_user_exists(self):
        result_1 = self.user_administration.check_if_username_exist("test_user_1")
        result_2 = self.user_administration.check_if_username_exist("uzer1")
        self.assertEqual(result_1, True)
        self.assertEqual(result_2, False)

