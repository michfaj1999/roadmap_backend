#Server.close() messages
SVR_SERVER_INVALID_REQUEST = b"invalid request.Type help to see server commands"
SVR_SERVER_CLOSE = b"close"
#Server.create_user() messages
SVR_CREATE_USER_ENTER_USER_NAME = b"Please enter user name:"
SVR_CREATE_USER_ENTER_PASSWRD = b"Please enter password:"
#Server.delete_user() messages
SVR_DELETE_ENTER_USER_NAME = b"Please enter user name to delete:"
#Server.login() messages
SVR_LOGIN_ENTER_USERNAME = b"Please enter user name:"
SVR_LOGIN_ENTER_PASSWORD = b"Please enter password:"
#Server.show_messagebox() messages
SVR_SHOW_MSGBOX_ENTER_USERNAME = b"please enter user name for messagebox display"
#Server.message() messages
SVR_MESSAGE_ENTER_USERNAME = b"please enter user name for receiver"
SVR_MESSAGE_ENTER_MESSAGE = b"please enter message for receiver"
#Server.delete_message() messages
SVR_DELETEMSG_MESSAGE_NUM = b"please enter number of message to delete"


#User_Administration CLASS

#User_Administration.create_user() messages
USR_ADMIN_CREATE_USER_INVALID_SIGNS = b"Invalid signs in username"
USR_ADMIN_CREATE_USER_NAME_ALREADY_EXIST = b"username already exist"
#User_Administration.delete_user() messages
USR_ADMIN_DELETE_USER_NOT_EXIST = b"user not exist"
USR_ADMIN_DELETE_USER_NO_PERMISSION = b"not permitted to delete users.Login as admin"
#User_Administration.login() messages
USR_ADMIN_LOGIN_LOGGED_IN = b"user logged in!" 
USR_ADMIN_LOGIN_INVALID = b"invalid login or password"
#User_Administration.show_messagebox() messages
USR_ADMIN_SHOW_MSGBOX_IS_EMPTY = b"messagebox is empty"
USR_ADMIN_SHOW_MSGBOX_LOGIN = b"please log in before display messagebox"
USR_ADMIN_SHOW_MSGBOX_INVALID_USER = b"invalid username"
#User_Administration.send_message() messages
USR_ADMIN_SND_MSG_LOGIN_BEFORE_SND = b"you have to log in before sending message"
USR_ADMIN_SND_MSG_INVALID_USERNAME = b"invalid sender name"
USR_ADMIN_SND_MSG_TOO_LONG_MSG = b"message too long.Up to 255 signs"
#User_Administration.send_message() messages
USR_ADMIN_DELETE_MSG_LOGIN = b"please log in before deleting messages"


#Client_Server_DB CLASS
#CLient_Server_DB.add_user_to_users_table() messages
DB_ADD_USER_ADDED = b"user added to users"
DB_ADD_USER_ERROR = b"Error while adding user"
#CLient_Server_DB.delete_user_from_users_table() messages
DB_DELETE_USER_DELETED = b"User deleted from users"
DB_DELETE_USER_ERROR = b"Error while deleting user"
#CLient_Server_DB.send_message() messages
DB_SEND_MSG_SEND = b"message has been send to the user"
DB_SEND_MSG_ERROR = b"error while sending message to user"
DB_SEND_MSG_FULL = b"messagebox is full"
DB_SEND_MSG_USR_DONT_EXIST = b"user doesn't exist"
#CLient_Server_DB.delete_message() messages
DB_DELETE_MSG_DELETED = b"data has been deleted"
DB_DELETE_MSG_ERROR = b"Error while deleting data"
DB_DELETE_MSG_INVALID_SIGNS = b"only numbers allowed"
DB_DELETE_MSG_VAL_OUTSIDE_RANGE = b"value outside messagebox range"
#Client_Server_DB.show_messagebox() messages
DB_SHOW_MSGBOX_EMPTY = b"messagebox is empty"
#CLient_Server_DB.get_users_list() messages
DB_GET_USR_LIST_ERROR = b"error while searching for users list"
#CLient_Server_DB.get_user_role() messages
DB_GET_USR_ROLE_ERROR = b"error while searching for user role"

