import unittest
from serverlib import ClientServerDB


class TestDataBase(unittest.TestCase):

    def prepare_test_case(self):
        self.test_queries = [
            ("INSERT INTO users (username,password,role) VALUES (%s,%s,%s);",("admin","password","admin")),
            ("INSERT INTO users (username,password,role) VALUES (%s,%s,%s);",("test_user_1","password","user")),
            ("INSERT INTO users (username,password,role) VALUES (%s,%s,%s);",("test_user_2","password","user")),
            ("INSERT INTO messages(username,sender,message) VALUES (%s,%s,%s);", ("test_user_1", "admin", "show_messagebox test message 1")),
            ("INSERT INTO messages(username,sender,message) VALUES (%s,%s,%s);", ("test_user_1", "admin", "show_messagebox test message 2")),
            ("INSERT INTO messages(username,sender,message) VALUES (%s,%s,%s);", ("test_user_2", "admin", "test_send_message_length message 1")),
            ("INSERT INTO messages(username,sender,message) VALUES (%s,%s,%s);", ("test_user_2", "admin", "test_send_message_length message 2")),
            ("INSERT INTO messages(username,sender,message) VALUES (%s,%s,%s);", ("test_user_2", "admin", "test_send_message_length message 3")),
            ("INSERT INTO messages(username,sender,message) VALUES (%s,%s,%s);", ("test_user_2", "admin", "test_send_message_length message 4")),
            ("INSERT INTO messages(username,sender,message) VALUES (%s,%s,%s);", ("test_user_2", "admin", "test_send_message_length message 5"))

        ]
        for test_query in self.test_queries:
            query = test_query[0]
            query_params = test_query[1]
            self.db.cursor.execute(query, query_params)
            self.db.connection.commit()

    def clear_data_after_test(self):
        query = "DELETE FROM users;"
        self.db.cursor.execute(query)
        self.db.connection.commit()
        query = "DELETE FROM messages;"
        self.db.cursor.execute(query)
        self.db.connection.commit()

    def check_if_record_exists_in_users_table(self, username, password, role):
        query = "SELECT COUNT(*) from users where username = %s and password = %s and role = %s;"
        query_params = (username, password, role)
        self.db.cursor.execute(query, query_params)
        result = self.db.cursor.fetchone()
        if int(result[0]) == 1:
            return True
        else:
            return False
    
    def check_if_message_exists_in_messages(self, username, sender, message):
        check_query = "SELECT COUNT(*) FROM messages WHERE username = %s and sender = %s and message = %s;"
        check_query_params = (username, sender, message)
        self.db.cursor.execute(check_query, check_query_params)
        result = int(self.db.cursor.fetchone()[0])
        print(result)
        if result == 1:
            return True
        elif result is None or result != 1:
            return False
    
    def setUp(self):
        self.filename = "client_server.ini"
        self.section = "test"
        self.db = ClientServerDB(self.filename, self.section)
        self.prepare_test_case()

    def tearDown(self):
        self.clear_data_after_test()
    
    def test_add_user_to_users_list(self):
        "Checks if user was added to users list"
        self.username = "add_user_test"
        self.password = "password"
        self.role = "user"
        self.db.add_user_to_users_table(self.username, self.password, self.role)
        self.assertEqual(self.check_if_record_exists_in_users_table(self.username, self.password, self.role), True)
     
    def test_delete_user_from_users_list(self):
        "Checks if user was deleted from users list"
        self.username = "test_user_1"
        self.password = "password"
        self.role = "user"
        self.db.delete_user_from_users_table(self.username)
        self.assertEqual(self.check_if_record_exists_in_users_table(self.username,self.password, self.role), False)

    def test_send_message(self):
        "Checks if message was send to user"
        self.receiver = "admin"
        self.sender = "test_user_1"
        self.message = "send message test"
        self.db.send_message(self.receiver, self.sender, self.message )
        self.assertEqual(
            self.check_if_message_exists_in_messages(self.receiver, self.sender, self.message), True
            )
        
    def test_send_message_to_not_existing_user(self):
        "Returns information about no existing user"
        self.receiver = "non_existing_name"
        self.sender = "test_user_1"
        self.message = "send message to non existing user"
        actual = self.db.send_message(self.receiver, self.sender, self.message)
        expected = b"user doesn't exist"
        self.assertEqual(actual, expected)

    def test_delete_message(self):
        "Checks if message was deleted from messagebox"
        self.username = "test_user_1"
        self.message_number_to_delete = '1'
        self.receiver = "test_user_1"
        self.sender = "admin"
        self.message = "test message"
        self.db.delete_message(self.username, self.message_number_to_delete)
        self.assertEqual(self.check_if_message_exists_in_messages(self.receiver, self.sender, self.message), False)
    
    def test_delete_message_with_number_outside_range(self):
        "Returns information about passing message number outside messagebox range"
        self.username = "admin"
        self.invalid_message_numer_to_delete = "-12"
        actual = self.db.delete_message(self.username, self.invalid_message_number_to_delete)
        expected = b"value outside messagebox range"
        self.assertEqual(actual, expected)

    def test_delete_message_invalid_literal(self):
        "Returns information about passing invalid number"
        self.username = "admin"
        self.invalid_message_number_to_delete = '#'
        actual = self.db.delete_message(self.username, self.invalid_message_number_to_delete )
        expected = b"only numbers allowed"
        self.assertEqual(actual, expected)

    def test_show_messagebox(self):
        "Checks if messagebox format is a list of tuple"
        self.username = "test_user_1"
        result = self.db.show_messagebox(self.username)
        if isinstance(result, list) and len(result) == 2 and isinstance(result[0], tuple):
            assert True
        else:
            assert False
        
    def test_get_username_and_password_correct(self):
        "Returns True for correct username and password"
        self.username = "admin"
        self.password = "password"
        self.assertEqual(self.db.get_username_and_password(self.username, self.password), True)
        
    def test_get_username_and_password_invalid(self):
        "Returns False for invalid username and password"
        self.username = "wrong_user"
        self.password = "wrong_password"
        self.assertEqual(self.db.get_username_and_password(self.username, self.password), False)
        
    def test_check_if_user_exists_correct(self):
        "Returns True for existing user"
        self.username = "admin"
        self.assertEqual(self.db.check_if_user_exist(self.username), True)
        
    def test_check_if_user_exists_invalid(self):
        "Returns False for unknown user"
        self.username = "non existing user"
        self.assertEqual(self.db.check_if_user_exist(self.username), False)

    def test_get_user_role(self):
        "Returns correct role for username"
        self.username = "admin"
        result = self.db.get_user_role(self.username)
        if result == "admin":
            assert True
        else:
            assert False
        
