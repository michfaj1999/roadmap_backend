import messages
import time
from datetime import datetime
import socket as s
import sys
import json
import psycopg2
import config
from psycopg2 import Error


class Server:

    def __init__(self, HOST, PORT, BUFFER, UserAdministration):
        self.HOST = HOST
        self.PORT = PORT
        self.BUFFER = BUFFER
        self.lifetime_start = time.time()
        self.start_date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        self.server_version = "v.1.1.4"
        self.close_flag = False
        self.commands = ("uptime", "info", "help", "close", "new", "delete",
                         "login", "messagebox", "message", "deletemsg", "users")
        self.user_administration = UserAdministration

    def uptime(self):
        "Returns server lifetime"
        live_time_stop = time.time()
        live_time = live_time_stop - self.lifetime_start

        dictionary = {
            "time": str(round(live_time, 2)),
        }
        return json.dumps(dictionary, ensure_ascii=False).encode("utf8")

    def help(self):
        "Returns list of commands avalible on the server"
        dictionary = {
            "uptime": "returns server lifetime",
            "info": "return server version and start date",
            "close": "close connection with server",
            "new": "adding new user",
            "login": "log in to your user account",
            "messagebox": "show messagebox",
            "message": "send message to another user",
            "deletemsg": "delete message from you messagebox",
            "users": "show users list"
        }
        return json.dumps(dictionary, ensure_ascii=False).encode("utf8")

    def info(self):
        "Returns information about server version and start date"
        dictionary = {
            "server_version": self.server_version,
            "start_date": str(self.start_date),
        }
        return json.dumps(dictionary, ensure_ascii=False).encode("utf8")

    def set_listening_socket(self):
        "Sets listening socket for connection"
        print("Server starting...")
        print()
        self.listening_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.listening_socket.bind((self.HOST, self.PORT))
        self.listening_socket.listen()

    def accept_client_socket(self):
        "Accepts incoming connection"
        self.client_socket, self.adress = self.listening_socket.accept()
        print(f"Connected to :{self.adress[0]} : {self.adress[1]} ")
        print()

    def serve_request(self):
        "Serves incoming requests in a loop"
        request = self.client_socket.recv(self.BUFFER).decode("utf8")
        if request in self.commands:
            match request:
                case "uptime":
                    msg = self.uptime()
                    self.client_socket.send(msg)
                case "info":
                    msg = self.info()
                    self.client_socket.send(msg)
                case "help":
                    msg = self.help()
                    self.client_socket.send(msg)
                case "close":
                    self.client_socket.send(messages.SVR_SERVER_CLOSE)
                    self.client_socket.shutdown(s.SHUT_RDWR)
                    self.client_socket.close()
                    print("Closing the server...")
                    sys.exit(0)
                case "new":
                    self.create_user()
                case "delete":
                    self.delete_user()
                case "login":
                    self.login()
                case "messagebox":
                    self.show_messagebox()
                case "message":
                    self.send_message()
                case "deletemsg":
                    self.delete_message()
                case "users":
                    self.users()
        
        else:
            self.client_socket.send(
                messages.SVR_SERVER_INVALID_REQUEST
                )

    def create_user(self):
        "Serves server logic for creating user"     
        self.client_socket.send(messages.SVR_CREATE_USER_ENTER_USER_NAME)
        name = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        self.client_socket.send(messages.SVR_CREATE_USER_ENTER_PASSWRD)
        password = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        result = self.user_administration.create_user(name, password)
        self.client_socket.send(result)

    def delete_user(self):
        "Serves server logic for deleting user"
        self.client_socket.send(messages.SVR_DELETE_ENTER_USER_NAME)
        name = self.client_socket.recv(self.BUFFER).decode("utf8")
        result = self.user_administration.delete_user(name)
        self.client_socket.send(result)

    def login(self):
        "Serves server logic for log user in"
        self.client_socket.send(messages.SVR_LOGIN_ENTER_USERNAME)
        name = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        self.client_socket.send(messages.SVR_LOGIN_ENTER_PASSWORD)
        password = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        result = self.user_administration.log_in(name, password)
        self.client_socket.send(result)
    
    def show_messagebox(self):
        "Serves server logic for showing user's messagebox"
        result = self.user_administration.show_messagebox()
        self.client_socket.send(result)
        
    def send_message(self):
        "Serves server logic for sending messages to users"
        self.client_socket.send(messages.SVR_MESSAGE_ENTER_USERNAME)
        name = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        self.client_socket.send(messages.SVR_MESSAGE_ENTER_MESSAGE)
        msg = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        result = self.user_administration.send_message(name, msg)
        self.client_socket.send(result)
        
    def delete_message(self):
        "Serves server logic for deleting messages from users messageboxes"
        self.client_socket.send(messages.SVR_DELETEMSG_MESSAGE_NUM)
        number = str(self.client_socket.recv(self.BUFFER).decode("utf8"))
        result = self.user_administration.delete_message(number)
        self.client_socket.send(result)

    def users(self):
        "Returns list of currently registered users"
        result = self.user_administration.get_users_list()
        self.client_socket.send(result)


class UserAdministration:

    def __init__(self, database):
        self.currently_logged_user = None
        self.db = database
        self.permissions = ["admin", "user"]
        self.not_permitted_username_signs = ["'", "'", "<", ">", "[", "{", "}", "]", ",", "."]
        self.MESSAGE_MAX_LENGTH = 256

    def create_user(self, username, password):
        "Create new user if user not exists and username is correct"
        if self.check_if_username_exist(username):
            return messages.USR_ADMIN_CREATE_USER_NAME_ALREADY_EXIST
        elif not self.check_username_correctness(username):
            return messages.USR_ADMIN_CREATE_USER_INVALID_SIGNS
        else:
            role = self.permissions[0] if username == self.permissions[0] else self.permissions[1]
            result = self.db.add_user_to_users_table(username, password, role)
            return result
              
    def delete_user(self, username):
        "If logged as admin and user exists - delete user"
        user_permissions = self.get_permissions(username)
        if user_permissions != self.permissions[0]:
            return messages.USR_ADMIN_DELETE_USER_NO_PERMISSION  
        elif not self.check_if_username_exist(username):
            return messages.USR_ADMIN_DELETE_USER_NOT_EXIST
        else:
            result = self.db.delete_user_from_users_table(username)
            return result
            
    def log_in(self, username, password):
        "If username and password is correct,log user in."
        check_result = self.db.get_username_and_password(username, password)
        if check_result:
            self.currently_logged_user = username
            return messages.USR_ADMIN_LOGIN_LOGGED_IN 
        return messages.USR_ADMIN_LOGIN_INVALID
          
    def get_permissions(self, username):
        "Get user permissions"
        return self.db.get_user_role(username)
                
    def check_username_correctness(self, username):
        "Check if passed username consists only permitted signs"
        for sign in username:
            if sign in self.not_permitted_username_signs:
                return False
        return True
    
    def check_if_username_exist(self, username):
        "Check if username already exists"
        result = self.db.check_if_user_exist(username)
        return result
        
    def get_users_list(self):
        "If list is not empty,return list of currently registered users"
        return json.dumps(self.db.get_users_list(), ensure_ascii=False).encode("utf8")
        
    def show_messagebox(self):
        "If user is logged in and user's messagebox is not empty -show user messagebox"
        username = self.currently_logged_user
        if username is None:
            return messages.USR_ADMIN_SHOW_MSGBOX_LOGIN
        else:
            result = self.db.show_messagebox(username)
            if isinstance(result, list):
                return json.dumps(result, ensure_ascii=False).encode("utf8")
            else:
                return result

    def send_message(self, receiver, message):
        "If sender is logged in and receiver exists and message is not too long - send message"
        sender = self.currently_logged_user
        if sender is None:
            return messages.USR_ADMIN_SND_MSG_LOGIN_BEFORE_SND
        elif not self.check_if_username_exist(receiver):
            return messages.USR_ADMIN_SND_MSG_INVALID_USERNAME
        elif len(message) > self.MESSAGE_MAX_LENGTH:
            return messages.USR_ADMIN_SND_MSG_TOO_LONG_MSG
        else:
            result = self.db.send_message(receiver, sender, message)
            return result

    def delete_message(self, number):
        "If user is logged in - delete message by message number"
        name = self.currently_logged_user
        if name is None:
            return messages.USR_ADMIN_DELETE_MSG_LOGIN
        return self.db.delete_message(name, number)


class ClientServerDB:

    def __init__(self, configfile, section):
        self.connect_to_database(configfile, section)
        self.MESSAGEBOX_MAX_LENGTH = 5
        self.DELETE_MESSAGE_MIN_VALUE = 0
        self.delete_message_permitted_signs = ("-", "+")

    def connect_to_database(self, configfile, section):
        "Get connection for database"
        params = config.config(configfile, section)
        try:
            self.connection = psycopg2.connect(**params)
            self.get_cursor()
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return error
                   
    def get_cursor(self):
        self.cursor = self.connection.cursor()

    def add_user_to_users_table(self, username, password, role):
        "Add username,password and role to users table"
        query = "INSERT INTO users (username,password,role) VALUES (%s,%s,%s);"
        query_params = (username, password, role)
        result = self.execute_query_without_return_data(query, query_params)
        if result:
            return messages.DB_ADD_USER_ADDED
        else:
            return messages.DB_ADD_USER_ERROR
        
    def delete_user_from_users_table(self, username):
        "delete user from users by username"
        query = "DELETE FROM users WHERE username = %s;"
        query_params = (username,)
        result = self.execute_query_without_return_data(query, query_params)
        if result:
            return messages.DB_DELETE_USER_DELETED 
        else:
            return messages.DB_DELETE_USER_ERROR

    def send_message(self, receiver, sender, message):
        "Send message if receiver exists and his messagebox is not full"
        if not self.check_if_user_exist(receiver):
            return messages.DB_SEND_MSG_USR_DONT_EXIST
        if self.get_messagebox_length(receiver) < self.MESSAGEBOX_MAX_LENGTH:
            query = "INSERT INTO messages (username,sender,message) VALUES (%s,%s,%s);"
            query_params = (receiver, sender, message)
            result = self.execute_query_without_return_data(query, query_params)
            if result:
                return messages.DB_SEND_MSG_SEND
            else:
                return messages.DB_SEND_MSG_ERROR
        else:
            return messages.DB_SEND_MSG_FULL
        
    def delete_message(self, username, delete_num):
        "if number to delete is correct,delete message from users messagebox"
        if not delete_num[1:].isdigit() or delete_num[0] not in self.delete_message_permitted_signs:
            return messages.DB_DELETE_MSG_INVALID_SIGNS
        if int(delete_num) < self.DELETE_MESSAGE_MIN_VALUE:
            return messages.DB_DELETE_MSG_VAL_OUTSIDE_RANGE
        query = "DELETE FROM messages WHERE username = %s and id = %s;"
        query_params = (username, delete_num)
        result = self.execute_query_without_return_data(query, query_params)
        if result:
            return messages.DB_DELETE_MSG_DELETED
        else:
            return messages.DB_DELETE_MSG_ERROR
        
    def show_messagebox(self, username):
        "Get user's messagebox by username"
        query = "Select id,sender,message from messages where username = %s;"
        query_params = (username,)
        user_messages = self.execute_query_with_return_data(query, query_params)
        if user_messages:
            return user_messages
        else:
            return messages.DB_SHOW_MSGBOX_EMPTY
        
    def get_username_and_password(self, username, password):
        "Check if username and password are correct"
        query = "SELECT (username,password) FROM users WHERE username = %s AND password = %s;"
        query_params = (username, password)
        result = self.execute_query_with_return_data(query, query_params)
        if result:
            return True
        else:
            return False
        
    def check_if_user_exist(self, username):
        "Check if user exist in users table"
        query = "SELECT EXISTS(SELECT 1 FROM users WHERE username = %s);"
        query_params = (username,)
        result = self.execute_query_with_return_data(query, query_params)
        if result is not None and result[0][0] is False:
            return False
        else:
            return True
        
    def get_users_list(self):
        "Get list of currently registered users"
        query = "SELECT (username,role) from users;"
        result = self.execute_query_with_return_data(query)
        if result:
            return result
        else:
            return messages.DB_GET_USR_LIST_ERROR
        
    def get_user_role(self, username):
        "Get user role by username"
        query = "SELECT role from users where username = %s;"
        query_params = (username,)
        result = self.execute_query_with_return_data(query, query_params)
        if result:
            return result[0][0]
        else:
            return messages.DB_GET_USR_ROLE_ERROR
        
    def get_messagebox_length(self, username):
        "Get user's messagebox lenght"
        query = "SELECT COUNT(*) FROM messages where username = %s;"
        query_params = (username,)
        return self.execute_query_with_return_data(query, query_params)[0][0]

    def execute_query_without_return_data(self, query, query_params=tuple()):
        try:
            self.cursor.execute(query, query_params)
            self.connection.commit()
            return True
        except Error as e:
            print(e)
            return False

    def execute_query_with_return_data(self, query, query_params=tuple()):
        try:
            self.cursor.execute(query, query_params)
            return self.cursor.fetchall()
        except Error as e:
            print(e)
            return None