from flask import Flask
from .routes import configure_routes
from threading import Lock


def create_app(Server):
    app = Flask(__name__)
    app.config["Server"] = Server
    configure_routes(app)
    return app


def run_flask(Server):
    app = create_app(Server)
    app.run(port=5000)
