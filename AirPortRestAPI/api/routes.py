from flask import jsonify


def configure_routes(app):
    @app.route('/start', methods=['POST'])
    def server_start():
        app.config['Server'].start()
        return jsonify(["Server is running"])
    
    @app.route('/freeze', methods=["POST"])
    def server_freeze():
        app.config['Server'].pause_event.clear()
        return jsonify(["Server is freezed"])
    
    @app.route('/restore', methods=["POST"])
    def server_continue():
        app.config['Server'].pause_event.set()
        return jsonify(['Server continue'])
    
    @app.route("/uptime", methods=['GET'])
    def server_uptime():
        response = {
            "uptime": app.config['Server'].uptime()
        }
        return jsonify(response)
    
    @app.route("/count", methods=['GET'])
    def server_currently_operating_airplanes_count():
        response = {
            "currently operating airplanes count": app.config['Server'].get_currently_operating_clients_count()
        }
        return jsonify(response)
    
    @app.route("/history/collisions", methods=['GET'])
    def server_collisions_historical_data():
        return jsonify(app.config['Server'].get_collisions_historical_data())
    
    @app.route("/history/landed", methods=['GET'])
    def server_landing_historical_data():
        return jsonify(app.config['Server'].get_landing_historical_data())
    
    @app.route("/list", methods=['GET'])
    def server_currently_operating_id_list():
        return jsonify(app.config["Server"].get_currently_operating_id_list())
    
    @app.route("/list/<int:plane_id>", methods=['GET'])
    def server_get_plane_info_by_plane_id(plane_id):
        return jsonify(app.config['Server'].get_plane_info_by_plane_id(plane_id))
    
    @app.route('/close', methods=['POST'])
    def server_stop():
        app.config['Server'].shutdown()
        return jsonify(['Server has been shutted down'])