import threading
from main_app.VisualizationStructures import Visu
from main_app.serverlib import Airport, Connection_Pool, ServerSockets, Airport_Database
from main_app.server import Server
from api import run_flask



#Connection parameters
HOST = '127.0.0.1'
PORT = 33000

SocketPool = ServerSockets(HOST, PORT)
Visualization = Visu()
Database = Airport_Database()
airport = Airport(Visualization, Database)
ConnectionPool = Connection_Pool(min_connections=5, max_connections=97, timeout=20, max_lifetime=20, idle_lifetime=20)
Main_Server = Server(HOST, PORT, SocketPool, ConnectionPool, airport, Visualization)
Flask_Thread = threading.Thread(target=run_flask, args=[Main_Server])
Flask_Thread.start()



