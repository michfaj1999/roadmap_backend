import threading
from threading import Lock
import serverlib
import socket as s
import json
from VisualizationStructures import Visu

server_Lock = Lock()
Lobby_lock = Lock()
Visu_lock = Lock()

HOST = '127.0.0.1'
PORT = 33000
SocketPool = serverlib.ServerSockets(HOST, PORT)


def Lobby_thread(lock):
    while True:
        socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        socket.bind((HOST, PORT))
        socket.listen()
        client_socket, adress = socket.accept()
        print("request accepted")
        with lock:
            if SocketPool.sockets_in_use.length() > 0:
                port = SocketPool.sockets_in_use.get(0).PORT
                print(port)
            else:
                if SocketPool.avalible_ports.length() > 0:
                    print("Adding port")
                    SocketPool.add_listening_socket()
                    port = SocketPool.sockets_in_use.get(0).PORT

                else:
                    print("No ports avalible")
                    client_socket.send(b"Max connections reached")
                    client_socket.close()
            client_socket.send(json.dumps(port, ensure_ascii=False).encode("utf8"))
            response = client_socket.recv(1024)
            if response == b"True":
                thread = threading.Thread(target=Server_thread, args=[server_Lock,])
                thread.start()
                client_socket.close()


def Server_thread(Lock):
    with Lock:
        socket = SocketPool.get_socket()
        database_connection = ConnectionPool.get_connection()
    if socket is None or database_connection is None:
        return
    else:
        with Lock:
            socket.accept_client_socket()
            plane_parameters = Airport.prepare_plane_parameters(socket.client_socket, database_connection)
            Airport.add_plane_to_currently_operating(plane_parameters)
            Airport.send_plane_id_to_plane(plane_parameters)
            Airport.add_airplane_to_runway_queue_by_start_position(plane_parameters)
        Airport.wait_for_landing(plane_parameters)
        Airport.fly_to_final_approach_starting_point(plane_parameters)
        Airport.landing(plane_parameters)
        SocketPool.return_free_port_number_to_avalible_ports(socket.PORT)
        ConnectionPool.release_connection(database_connection)      


Visu = Visu()
Airport = serverlib.Airport(Visu)
ConnectionPool = serverlib.Connection_Pool(min_connections=5, max_connections=97, timeout=20, max_lifetime=20,idle_lifetime=20)
Lobby = threading.Thread(target=Lobby_thread, args=[Lobby_lock,])
Lobby.start()

Airport_runway_control = threading.Thread(target=Airport.runway_queues_control_loop)
Airport_runway_control.start()

Visualization_Thread = threading.Thread(target=Visu.VisuThread, args=[Visu_lock, Visu])
Visualization_Thread.start()


