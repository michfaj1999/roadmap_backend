import unittest
from unittest.mock import patch
from serverlib import Airport_Database, ServerSockets, Connection_Pool


class TestServerSockets(unittest.TestCase):

    def setUp(self):
        HOST = '0.0.0.0'
        PORT = 30000
        self.serversockets = ServerSockets(HOST, PORT, min_connections=0)
        
    def tearDown(self):
        del self.serversockets

    def test_prepare_avalible_ports_list(self):
        self.serversockets.min_connections = 1
        self.serversockets.prepare_avalible_ports_list()
        self.assertEqual(self.serversockets.avalible_ports.length, 1)
    
    @patch('serverlib.Socket')
    def test_add_listening_socket(self, MockSocket):
        self.serversockets.add_listening_socket()
        self.assertEqual(self.serversockets.sockets_in_use.length(), 1)
    
    def test_get_socket_if_avalible_sockets_is_empty(self):
        socket = self.serversockets.get_socket()
        self.assertEqual(socket is not None, True)

    def test_return_free_port_number_to_avalible_ports(self):
        PORT = 12345
        self.serversockets.return_free_port_number_to_avalible_ports(PORT)
        self.assertEqual(PORT in self.serversockets.avalible_ports, True)


class TestConnectionPool(unittest.TestCase):
    def setUp(self):
        self.connection_pool = Connection_Pool(min_connections=0, max_connections=1, max_lifetime=1000, timeout=30, idle_lifetime=30)
    
    def tearDown(self):
        del self.connection_pool
    
    def test_get_connection(self):
        connection = self.connection_pool.get_connection()
        self.assertEqual(connection is not None, True)
    
    def test_add_connection_to_avalible(self):
        self.connection_pool.add_connection_to_available()
        self.assertEqual(self.connection_pool.available_connections.length() == 1, True)
    
    @patch('serverlib.Airport_Database')
    def test_move_avalible_connection_to_used(self, connection):
        self.connection_pool.available_connections.append(connection)
        self.connection_pool.move_available_connection_to_used()
        self.assertEqual(connection in self.connection_pool.connections_in_use, True)
        
    @patch('serverlib.Airport_Database')
    def test_release_connection(self, connection):
        self.connection_pool.release_connection(connection)
        self.assertEqual(connection in self.connection_pool.available_connections, True)


class TestAirportDatabase(unittest.TestCase):

    def setUp(self):
        self.database = Airport_Database()
    
    def tearDown(self):
        del self.database
    
    




