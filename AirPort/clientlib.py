import time
import socket as s
from serverlib import CommunicationProtocol
import random
import numpy as np
from threading import Lock


class Airplane:

    def __init__(self, HOST, PORT):
        self.HOST = HOST
        self.PORT = PORT
        self.socket = None
        self.create_connection()
        self.plane_id = None
        self.communication = CommunicationProtocol(self.socket)
        self.restricted_spawn_area_x = range(4000, 7000)
        self.restricted_spawn_area_y = range(7000, 10000)
        self.position = np.array([])
        self.velocity = np.array([50, 50, 50])
        self.direction = np.array([1, 0, 0])
        self.target_position = np.array([0, 0, 0])
        self.plane_lock = Lock()
        self.stop_flag = False
        self.message_lock = Lock()
        self.is_airplane_crashed = False
        self.priority_flag = False

    def listen_messages(self):
        with self.message_lock:
            if self.is_airplane_crashed is False:
                try:
                    message = self.communication.receive_message()
                except ConnectionAbortedError:
                    return
            else:
                message = None
            if message is not None:
                message_type, data = message[0], message[1]
                if message_type == 'PLANE_ID':
                    self.plane_id = data
                elif message_type == 'TARGET_RUNWAY':
                    self.target_runway = data
                elif message_type == 'TARGET_POSITION':
                    self.target_position = np.array(data)
                elif message_type == "STOP_FLAG":
                    self.stop_flag = True
                elif message_type == "COLLISION":
                    self.is_airplane_crashed = True
                    print("airplane {} has crashed".format(self.plane_id))
                elif message_type == "COLLISION DANGER":
                    self.perform_crash_avoidant_maneuver(data)
                elif message_type == "PRIORITY_FLAG":
                    self.priority_flag = data
                    
                # Add handling for other message types as needed
            else:
                return

    def create_connection(self):
        self.socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.socket.connect((self.HOST, self.PORT))
        port = self.socket.recv(1024).decode("utf-8")
        port = int(port)
        if port == b"Max connections reached":
            self.socket.close()
            self.PORT = None
            return
        self.socket.send(b"True")
        self.socket.close()
        self.socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.socket.connect((self.HOST, port))
        response = self.socket.recv(1024).decode("utf-8")
        print(response)
        print("connection succesfull")
        self.is_plane_on_position = [False, False, False]

    def set_start_position_on_field(self):
        border_lines = ("x0", "y0", "x10k", "y10k")
        border = random.choice(border_lines)
        if border == "x0":
            x = 0
            y = random.randint(0, 10000)
            while y in self.restricted_spawn_area_y:
                y = random.randint(0, 10000)    
        elif border == "y0":
            y = 0
            x = random.randint(0, 10000)
            while x in self.restricted_spawn_area_x:
                x = random.randint(0, 10000)
        if border == "x10k":
            x = 10000
            y = random.randint(0, 10000)
            while y in self.restricted_spawn_area_y:
                y = random.randint(0, 10000)    
        elif border == "y10k":
            y = 10000
            x = random.randint(0, 10000)
            while x in self.restricted_spawn_area_x:
                x = random.randint(0, 10000)
        z = random.randint(2000, 5000)
        self.position = np.array([float(x), float(y), float(z)])

    def send_plane_position(self):
        self.communication.send_message(['PLANE_POSITION', self.position.tolist()])

    def update_position(self, target_position=None, delta_time=1.0):
        target_position = self.target_position

        # Calculate direction vector towards the target position
        target_direction = target_position - self.position
        target_distance = np.linalg.norm(target_direction)
        if target_distance > 0:  # Avoid division by zero
            self.direction = target_direction / target_distance

        # Update velocity towards the target point
        x_velocity = random.randint(0, 70)
        y_velocity = random.randint(0, 70)
        z_velocity = random.randint(0, 70)
        self.velocity = np.array([x_velocity, y_velocity, z_velocity])
        speed = np.linalg.norm(self.velocity)
        self.velocity = self.direction * speed

        # Update position using constant velocity: s = vt
        self.position += self.velocity * delta_time 
        if self.position[2] < 0:
            self.position[2] = 0.00
    
    def perform_crash_avoidant_maneuver(self, position_of_other_plane):
        # Calculate direction away from the other plane
        direction_away = self.position - np.array(position_of_other_plane)
        # Normalize direction vector
        direction_away /= np.linalg.norm(direction_away)
        # Apply a small adjustment to current position to move away from the other plane
        avoidance_distance = 170  # Adjust this value as needed
        self.position += direction_away * avoidance_distance
        self.priority_flag = False

    def fly_loop(self):
     
        while not self.stop_flag and not self.is_airplane_crashed: #Waiting for landing
            with self.plane_lock:
                self.update_position()
                self.send_plane_position()
                self.listen_messages()
                if self.is_airplane_crashed is True:
                    break
                time.sleep(0.3)

        if not self.is_airplane_crashed:
            with self.plane_lock:
                self.stop_flag = False
            self.listen_messages()
 
        while not self.stop_flag and not self.is_airplane_crashed: #fly to final approach startpoint
            with self.plane_lock:
                self.update_position()
                self.send_plane_position()
                self.listen_messages()
                if self.is_airplane_crashed is True:
                    break
                time.sleep(0.3)

        if not self.is_airplane_crashed:
            with self.plane_lock:
                self.stop_flag = False
            self.listen_messages()

        while not self.stop_flag and not self.is_airplane_crashed: #Landing
            with self.plane_lock:
                self.update_position()
                self.send_plane_position()
                if self.is_airplane_crashed is True:
                    break
                self.listen_messages()
                
                time.sleep(0.3)
        print("plane", self.plane_id,"has landed sucessfully")
        self.socket.close()
        return
