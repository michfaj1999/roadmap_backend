import socket as s
from threading import Lock
import random
import json
import time
import queue
import sqlite3
from sqlite3 import Error
from datetime import timedelta
import schedule
from datetime import datetime
from ThreadSafeStructures import ThreadSafeDict, ThreadSafeList


class Socket:
    def __init__(self, HOST, PORT):
        self.HOST = HOST
        self.PORT = PORT
        self.listening_socket = None
        self.client_socket = None
        self.client_adress = None

    def set_listening_socket(self):
        self.listening_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.listening_socket.bind((self.HOST, self.PORT))
        self.listening_socket.listen()

    def accept_client_socket(self):
        self.client_socket, self.client_adress = self.listening_socket.accept()
        self.client_socket.send(b"confirm")
        print("accepted")


class ServerSockets:
    #Pool of sockets, managing connections on avalible ports
    def __init__(self, HOST, PORT, min_connections=5, max_connections=100):
        self.HOST = HOST
        self.FIRST_PORT = PORT + 1
        self.min_connections = 5
        self.max_connections = 100
        self.sockets_in_use = ThreadSafeList()
        self.avalible_ports = ThreadSafeList()
        self.prepare_avalible_ports_list()
        for i in range(self.min_connections):
            self.add_listening_socket()

    def prepare_avalible_ports_list(self):
        for i in range(self.max_connections):
            port = self.FIRST_PORT + i
            self.avalible_ports.append(port)
     
    def add_listening_socket(self):
        port = self.avalible_ports.pop(0)
        socket = Socket(self.HOST, port)
        socket.set_listening_socket()
        self.sockets_in_use.append(socket)

    def get_socket(self):
        if self.sockets_in_use.length() > 0:
            socket = self.sockets_in_use.pop(0)
            return socket
        else:
            if self.avalible_ports.length() > 0:
                self.add_listening_socket()
                socket = self.sockets_in_use.pop(0) 
                return socket
            else:
                return None
            
    def return_free_port_number_to_avalible_ports(self, port):
        self.avalible_ports.append(port)


class PlaneParameters:
    def __init__(self, plane_id, communication, database_connection, runway=None, position=None):
        self.plane_id = plane_id
        self.communication = communication
        self.runway = runway
        self.position = position
        self.flight_start_time = datetime.now()
        self.database_connection = database_connection
        self.plane_lock = Lock()
        self.is_plane_crashed = False
        self.has_priority = False


class Airport:

    def __init__(self, Visu, Database=None):
        self.restricted_spawn_area_x = range(4000, 7000)
        self.restricted_spawn_area_y = range(7000, 10000)
        self.airport_area_x_size_left = 0
        self.airport_area_x_size_right = 10000
        self.airport_area_y_size_left = 0
        self.airport_area_y_size_right = 10000
        self.currently_operating_planes = ThreadSafeDict()
        self.runway_A_approach_path_end_point = (4850, 9000, 0)
        self.runway_A_approach_path_starting_point = (4850, 1000, 2000)
        self.runway_B_approach_path_end_point = (5050, 9000, 0)
        self.runway_B_approach_path_starting_point = (5050, 1000, 2000)
        self.currently_on_runway_A = None
        self.currently_on_runway_B = None
        self.Database = Database
        self.Visu = Visu
        #Queues for managing airplanes FIFO order
        self.runway_A_queue = queue.Queue()
        self.runway_B_queue = queue.Queue()
        #Lock for thread safety when accesing runway queues
        self.queue_lock = Lock()

    def prepare_plane_parameters(self, socket, database_connection):
        plane_id = len(self.currently_operating_planes) + 1
        plane_communication = CommunicationProtocol(socket)
        plane_parameters = PlaneParameters(plane_id, plane_communication, database_connection)
        return plane_parameters
    
    def add_plane_to_currently_operating(self, plane_parameters):
        self.currently_operating_planes[plane_parameters.plane_id] = plane_parameters
        plane_parameters.database_connection.add_event_to_events_table(plane_parameters.plane_id,"Connected to Airport", 0, 0, 0)
        
    def remove_plane_from_currently_operating(self, plane_parameters):
        del self.currently_operating_planes[plane_parameters.plane_id]
    
    def send_plane_id_to_plane(self, plane_parameters):
        plane_parameters.communication.send_message(['PLANE_ID', plane_parameters.plane_id])

    def send_target_position_to_plane(self, target_position, communication):
        communication.send_message(['TARGET_POSITION', target_position])

    def send_stop_looping_flag(self, plane_parameters):
        plane_parameters.communication.send_message(['STOP_FLAG', None])
    
    def send_collision_danger_flag(self, plane_parameters, plane_position):
        plane_parameters.communication.send_message(["COLLISION DANGER", plane_position])

    def send_collision_flag(self, plane_parameters):
        plane_parameters.communication.send_message(["COLLISION", None])
    
    def send_priority_flag(self, plane_parameters):
        plane_parameters.communication.send_message(["PRIORITY_FLAG", True])

    def get_plane_position(self, communication):
        received_message = None
        while received_message is None:
            received_message = communication.receive_message()
        if received_message is None:
            return None
        elif received_message[0] == 'PLANE_POSITION':
            return received_message[1]
        
    def choose_final_approach_point_by_runway(self, runway):
        if runway == 'A':
            return self.runway_A_approach_path_starting_point
        if runway == 'B':
            return self.runway_B_approach_path_starting_point
        
    def check_maximum_flight_time(self, plane_parameters):
        if (datetime.now() - plane_parameters.flight_start_time) > timedelta(seconds=7200):
            print("maximum flight time:",plane_parameters.plane_id)
            plane_parameters.is_plane_crashed = True
            self.send_collision_flag(plane_parameters)
            if self.currently_on_runway_A == plane_parameters.plane_id:
                self.currently_on_runway_A = None
            if self.currently_on_runway_B == plane_parameters.plane_id:
                self.currently_on_runway_B = None 
            self.remove_plane_from_currently_operating(plane_parameters)
            plane_parameters.database_connection.add_event_to_events_table(plane_parameters.plane_id,"Airplane has crashed",plane_parameters.position[0], plane_parameters.position[1], plane_parameters.position[2])

    def check_if_plane_is_on_position(self, position, target_position):
        x = int(position[0])
        y = int(position[1])
        z = int(position[2])
        x_target = int(target_position[0])
        y_target = int(target_position[1])
        z_target = int(target_position[2])

        is_plane_on_position = [False, False, False]
        if int(x) in range(x_target - 100, x_target + 100):
            is_plane_on_position[0] = True
        if int(y) in range(y_target - 100, y_target + 100):
            is_plane_on_position[1] = True
        if int(z) in range(z_target - 100, z_target + 100):
            is_plane_on_position[2] = True
        
        if is_plane_on_position[0] and is_plane_on_position[1] and is_plane_on_position[2]:
            return True
        else:
            return False
    
    def check_distance_to_other_planes(self, plane_parameters):
        x = plane_parameters.position[0]
        y = plane_parameters.position[1]
        z = plane_parameters.position[2]

        for plane_id in self.currently_operating_planes.keys():
            if plane_parameters.plane_id != plane_id:
                plane_to_check_parameters = self.currently_operating_planes[plane_id]
                if plane_to_check_parameters.position is None:
                    return
                check_x = plane_to_check_parameters.position[0]
                check_y = plane_to_check_parameters.position[1]
                check_z = plane_to_check_parameters.position[2]
                if plane_parameters.is_plane_crashed == True or plane_to_check_parameters.is_plane_crashed == True:
                    return
                if (abs(x-check_x) < 100 or abs(y-check_y) < 100) and abs(z - check_z) < 100:
                    if plane_to_check_parameters.has_priority == False:
                        plane_parameters.has_priority = True
                        self.send_priority_flag(plane_parameters)
                    elif plane_to_check_parameters.has_priority == True:
                        return
                    self.send_collision_danger_flag(plane_parameters, plane_to_check_parameters.position)
    
    def check_for_collision(self, plane_parameters):
        x = plane_parameters.position[0]
        y = plane_parameters.position[1]
        z = plane_parameters.position[2]

        for plane_id in self.currently_operating_planes.keys():
            if plane_parameters.plane_id != plane_id:
                plane_to_check_parameters = self.currently_operating_planes[plane_id]
                if plane_to_check_parameters.position is None:
                    return
                check_x = plane_to_check_parameters.position[0]
                check_y = plane_to_check_parameters.position[1]
                check_z = plane_to_check_parameters.position[2]
                if plane_parameters.is_plane_crashed == True or plane_to_check_parameters.is_plane_crashed == True:
                    return
                
                if (abs(x-check_x) < 10 or abs(y-check_y) < 10) and abs(z - check_z) < 10:
                    print("collision",plane_parameters.plane_id, plane_id)
                    #Set collision flag for True
                    plane_parameters.is_plane_crashed = True
                    plane_to_check_parameters.is_plane_crashed = True
                    #Send collision flag to the airplanes
                    self.send_collision_flag(plane_parameters)
                    self.send_collision_flag(plane_to_check_parameters)
                    time.sleep(2)
                    #Clear runways if any of airplanes was during landing
                    if self.currently_on_runway_A == plane_parameters.plane_id or self.currently_on_runway_A == plane_to_check_parameters.plane_id:
                        self.currently_on_runway_A = None
                    if self.currently_on_runway_B == plane_parameters.plane_id or self.currently_on_runway_B == plane_to_check_parameters.plane_id:
                        self.currently_on_runway_B = None 
                    #Remove from currently operating list
                    self.remove_plane_from_currently_operating(plane_parameters)
                    self.remove_plane_from_currently_operating(plane_to_check_parameters)
                    #Remove from visu
                    self.Visu.delete_airplane_from_visu(plane_parameters)
                    self.Visu.delete_airplane_from_visu(plane_to_check_parameters)
                    #Add crash event to database
                    plane_parameters.database_connection.add_event_to_events_table(plane_parameters.plane_id,"Airplane has crashed",plane_parameters.position[0], plane_parameters.position[1],plane_parameters.position[2])
                    plane_parameters.database_connection.add_event_to_events_table(plane_to_check_parameters.plane_id,"Airplane has crashed",plane_to_check_parameters.position[0], plane_to_check_parameters.position[1],plane_to_check_parameters.position[2])
                
    def add_airplane_to_runway_queue_by_start_position(self, plane_parameters):
        while plane_parameters.position is None:
            plane_parameters.position = self.get_plane_position(plane_parameters.communication)
        self.Visu.add_airplane_to_visu(plane_parameters)
        plane_parameters.runway = 'A' if plane_parameters.position[0] < 5000 else 'B'

        if plane_parameters.runway == 'A':
            self.runway_A_queue.put(plane_parameters)
            plane_parameters.database_connection.add_event_to_events_table(plane_parameters.plane_id,"Airplane has been added to runway A queue",plane_parameters.position[0], plane_parameters.position[1],plane_parameters.position[2])

        elif plane_parameters.runway == 'B':
            self.runway_B_queue.put(plane_parameters)
            plane_parameters.database_connection.add_event_to_events_table(plane_parameters.plane_id,"Airplane has been added to runway B queue",plane_parameters.position[0], plane_parameters.position[1],plane_parameters.position[2])

    def get_target_point_while_waiting(self, plane_parameters):
        if plane_parameters.runway == 'A':
            x = random.uniform(0, 4000)
            y = random.uniform(0, 7000)
            z = plane_parameters.position[2]
            return [x, y, z]
        
        if plane_parameters.runway == 'B':
            x = random.uniform(7000, 10000)
            y = random.uniform(0, 7000)
            z = plane_parameters.position[2]
            return [x, y, z]

    def wait_for_landing(self, plane_parameters):

        if plane_parameters.is_plane_crashed:
            return
        
        with plane_parameters.plane_lock:
            target_position = self.get_target_point_while_waiting(plane_parameters)
            self.send_target_position_to_plane(target_position, plane_parameters.communication)
            is_plane_on_position = False

        while self.currently_on_runway_A != plane_parameters.plane_id and self.currently_on_runway_B != plane_parameters.plane_id and not plane_parameters.is_plane_crashed:
            with plane_parameters.plane_lock:
                if plane_parameters.is_plane_crashed is True:
                    break
                if is_plane_on_position:
                    target_position = self.get_target_point_while_waiting(plane_parameters)
                    self.send_target_position_to_plane(target_position, plane_parameters.communication)
                    is_plane_on_position = False
                plane_parameters.position = self.get_plane_position(plane_parameters.communication)
                self.check_maximum_flight_time(plane_parameters)
                self.check_distance_to_other_planes(plane_parameters)
                self.check_for_collision(plane_parameters)
                is_plane_on_position = self.check_if_plane_is_on_position(plane_parameters.position, target_position)
                self.Visu.update_airplane_position(plane_parameters)
                self.Visu.scale_airplane_avatar_size_by_attitude(plane_parameters)

        if not plane_parameters.is_plane_crashed:
            with plane_parameters.plane_lock:
                self.send_stop_looping_flag(plane_parameters)
            return
        else:
            return
    
    def fly_to_final_approach_starting_point(self, plane_parameters):
        if plane_parameters.is_plane_crashed:
            return
        #Phase 1: approaching final aproach statring point
        plane_parameters.database_connection.add_event_to_events_table(plane_parameters.plane_id, "Plane flying to final approach starting point", plane_parameters.position[0], plane_parameters.position[1], plane_parameters.position[2])

        with plane_parameters.plane_lock:
            target_position = self.choose_final_approach_point_by_runway(plane_parameters.runway)
            self.send_target_position_to_plane(target_position, plane_parameters.communication)
        is_plane_on_position = False

        while not is_plane_on_position and not plane_parameters.is_plane_crashed:
            with plane_parameters.plane_lock:
                if plane_parameters.is_plane_crashed is True:
                    break
                plane_parameters.position = self.get_plane_position(plane_parameters.communication)
                self.check_maximum_flight_time(plane_parameters)
                self.check_distance_to_other_planes(plane_parameters)
                self.check_for_collision(plane_parameters)
                is_plane_on_position = self.check_if_plane_is_on_position(plane_parameters.position, target_position)
                self.Visu.update_airplane_position(plane_parameters)
                self.Visu.scale_airplane_avatar_size_by_attitude(plane_parameters)

        if not plane_parameters.is_plane_crashed:
            with plane_parameters.plane_lock:
                self.send_stop_looping_flag(plane_parameters)
            return
        else:
            return
    
    def landing(self, plane_parameters):
        #Phase 2: Final
        if plane_parameters.is_plane_crashed:
            return
        plane_parameters.database_connection.add_event_to_events_table(plane_parameters.plane_id, "Plane is on a final approach", plane_parameters.position[0], plane_parameters.position[1], plane_parameters.position[2])

        with plane_parameters.plane_lock:
            if plane_parameters.runway == 'A':
                target_position = self.runway_A_approach_path_end_point
            if plane_parameters.runway == 'B':
                target_position = self.runway_B_approach_path_end_point
            is_plane_on_position = False
            self.send_target_position_to_plane(target_position, plane_parameters.communication)
            
        while not is_plane_on_position and not plane_parameters.is_plane_crashed:
            with plane_parameters.plane_lock:
                if plane_parameters.is_plane_crashed is True:
                    break
                plane_parameters.position = self.get_plane_position(plane_parameters.communication)
                self.check_maximum_flight_time(plane_parameters)
                self.check_distance_to_other_planes(plane_parameters)
                self.check_for_collision(plane_parameters)
                is_plane_on_position = self.check_if_plane_is_on_position(plane_parameters.position, target_position)
                self.Visu.update_airplane_position(plane_parameters)
                self.Visu.scale_airplane_avatar_size_by_attitude(plane_parameters)
                
        if not plane_parameters.is_plane_crashed:
            plane_parameters.database_connection.add_event_to_events_table(plane_parameters.plane_id, "Plane has been landed sucessfully", plane_parameters.position[0], plane_parameters.position[1], plane_parameters.position[2])

            #Relase runway after landing
        with plane_parameters.plane_lock:
            if not plane_parameters.is_plane_crashed:
                self.remove_plane_from_currently_operating(plane_parameters)
                self.send_stop_looping_flag(plane_parameters)
            if plane_parameters.runway == 'A':
                self.currently_on_runway_A = None
            if plane_parameters.runway == 'B':
                self.currently_on_runway_B = None
        return

    def runway_queues_control_loop(self):
        #controls the queue of landing
        while True:
            if not self.runway_A_queue.empty():
                if self.currently_on_runway_A is None:
                    with self.queue_lock:
                        plane_parameters = self.runway_A_queue.get()
                        if not plane_parameters.is_plane_crashed:
                            self.currently_on_runway_A = plane_parameters.plane_id
                            print("wybrano samolot do ladowania na pasie A:", plane_parameters.plane_id)
                        else:
                            pass
            if not self.runway_B_queue.empty():
                if self.currently_on_runway_B is None:
                    with self.queue_lock:
                        plane_parameters = self.runway_B_queue.get()
                        if not plane_parameters.is_plane_crashed:
                            self.currently_on_runway_B = plane_parameters.plane_id
                            print("wybrano samolot do ladowania na pasie B:", plane_parameters.plane_id)
                        else:
                            pass


class CommunicationProtocol:
    def __init__(self, socket):
        self.socket = socket
        self.received_buffer = b""
        self.send_buffer = b""
        self.header_length = 2
       
    def send_message(self, data):
        json_data = json.dumps(data, ensure_ascii=False).encode("utf8")
        json_length = len(json_data).to_bytes(2, "big")
        message = json_length + json_data
        self.send_buffer += message
        while len(self.send_buffer) != 0:
            try:
                sended = self.socket.send(self.send_buffer)
                self.send_buffer = self.send_buffer[sended:]
            except ConnectionAbortedError:
                return
            
    def receive_message(self):
        self.socket.setblocking(False)
        while len(self.received_buffer) <= self.header_length:
            try:
                data = self.socket.recv(4096)
            except BlockingIOError:
                return None
            except ConnectionAbortedError:
                return None
            if data != 0:
                self.received_buffer += data
        self.socket.setblocking(True)
        json_length = int.from_bytes(self.received_buffer[:self.header_length],"big")
        self.received_buffer = self.received_buffer[self.header_length:]
        while len(self.received_buffer) < json_length:
            self.received_buffer += self.socket.recv(4096)
        received_message = json.loads(self.received_buffer[:json_length].decode("utf-8"))
        self.received_buffer = self.received_buffer[json_length:]
        return received_message


class Airport_Database:
    
    def __init__(self, dbfile=r"DB_Airport"):
        self.dbfile = dbfile
        self.connection = None
        self.cursor = None
        self.create_connection()
        self.create_events_table()

    def create_connection(self):
        "Creating a database connection for SQLite"
        try:
            self.connection = sqlite3.connect(database=self.dbfile, check_same_thread=False)
            self.get_cursor()

        except Error as e:
            print(e)
            return e
    
    def get_cursor(self):
        "Get connection cursor"
        self.cursor = self.connection.cursor()
    
    def create_events_table(self):
        "Create table of airplanes"
        query = """ CREATE TABLE IF NOT EXISTS events (
                                        event_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        plane_id INTEGER,
                                        event TEXT,
                                        x INTEGER,
                                        y INTEGER,
                                        z INTEGER
                                    ); """
        self.execute_query_without_return_data(query)

    def add_event_to_events_table(self, plane_id, event, x, y, z):
        query = "INSERT INTO events(plane_id,event,x,y,z) VALUES (?,?,?,?,?);"
        data = (plane_id, event, x, y, z)
        self.execute_query_without_return_data(query, data)
    
    def execute_query_with_return_data(self, query, data=tuple()):
        "Execute queries which return data"
        try:
            self.cursor.execute(query, data)
            result = self.cursor.fetchall()
            return result
        except Error as e:
            print(e)
            return None

    def execute_query_without_return_data(self, query, data=tuple()):
        "Execute queries which don't return data"
        try:
            self.cursor.execute(query, data)
            self.connection.commit()
            return True
        except Error as e:
            self.connection.rollback()
            print(e)
            return False


class Connection_Pool(Airport_Database):
    def __init__(self, min_connections, max_connections, timeout, max_lifetime, idle_lifetime):
        self.min_connections = min_connections
        self.max_connections = max_connections
        self.available_connections = ThreadSafeList()
        self.connections_in_use = ThreadSafeList()
        self.lock = Lock()
        self.timeout = float(timeout)
        self.max_lifetime = timedelta(minutes=max_lifetime)
        self.idle_lifetime = timedelta(minutes=idle_lifetime)
        for _ in range(self.min_connections):
            self.add_connection_to_available()
        schedule.every(1).minute.do(self.manage_connections)
    
    def get_connection(self):
        start_time = time.time()
        while time.time() - start_time < self.timeout:
            if self.available_connections.length() > 0:
                connection = self.move_available_connection_to_used() 
                return connection
            else:
                if self.available_connections.length() + self.connections_in_use.length() < self.max_connections:
                    print("No available connections.Adding another connection.")
                    self.add_connection_to_available()
                    connection = self.move_available_connection_to_used()
                    return connection
                else:
                    print("Too many connections in use.Waiting for an available connection")
                    time.sleep(1)
        print("Connection timeout reached. Unable to acquire connection.")
        return None
            
    def add_connection_to_available(self):
        connection = Airport_Database()
        idle_time = datetime.now()
        if connection.connection is None:
            return
        self.available_connections.append((connection, idle_time))
    
    def move_available_connection_to_used(self):
        connection_tuple = self.available_connections.pop(0)
        connection, idle_time = connection_tuple
        self.connections_in_use.append(connection)
        return connection
    
    def release_connection(self, connection):
        self.connections_in_use.remove(connection)
        idle_time = datetime.now()
        self.available_connections.append((connection, idle_time))
    
    def destroy_unused_connections(self):
        print("checking for too many unused connections")
        now = datetime.now()
        with self.lock:
            while self.available_connections.length() > self.min_connections :
                connection_tuple = self.available_connections.get(0)
                connection, idle_time = connection_tuple
                if (now - connection.creation_time).total_seconds() > self.max_lifetime.total_seconds():
                    connection.connection.close()
                    self.available_connections.remove(connection_tuple)
                    
                    print("Connection closed due to max lifetime")
                if (now - idle_time).total_seconds() > self.idle_lifetime.total_seconds():
                    self.available_connections.remove(connection_tuple)
                    print("Connection closed due to idle")
                else:
                    break

    def run_health_check(self):
        print("Running health check for connections.")
        with self.lock:
            for connection_tuple in self.available_connections:
                connection, _ = connection_tuple
                if not self.is_connection_valid(connection):
                    print("Connection failed health check:", connection)
                    connection.connection.close()
                    self.available_connections.remove(connection_tuple)
                    self.add_connection_to_available()
                    
    def is_connection_valid(self, connection):
        try:
            with connection.connection.cursor() as cursor:
                cursor.execute("SELECT 1")
                return True
        except (Exception, Error) as error:
            print("Error during health check:", error)
            return False
          
    def manage_connections(self):
        self.destroy_unused_connections()
        self.run_health_check()

    def run_scheduler(self):
        print("Manage connection scheduler on")
        while True:
            schedule.run_pending()
            time.sleep(1)

    def stop_scheduler(self):
        print("Scheduler stopped")

