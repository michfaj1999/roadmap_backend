import clientlib
import time
import threading
from threading import Lock

HOST = '127.0.0.1'
PORT = 33000

lock = Lock()


def mainThread(lock):
    with lock:
        airplane = clientlib.Airplane(HOST, PORT)
        airplane.listen_messages()
        airplane.set_start_position_on_field()
        airplane.send_plane_position()
        if airplane.PORT is None:
            return
    airplane.fly_loop()


for i in range(15):
    thread = threading.Thread(target=mainThread, args=[lock, ])
    thread.start()
    time.sleep(1)
