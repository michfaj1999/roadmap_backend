from threading import Lock


class ThreadSafeList:
    def __init__(self):
        self._list = list()
        self._lock = Lock()
 
    def append(self, value):
        with self._lock:
            self._list.append(value)

    def pop(self, value=None):
        if value is None:
            with self._lock:
                return self._list.pop()
        else:
            with self._lock:
                return self._list.pop(value)

    def get(self, index):
        with self._lock:
            return self._list[index]
    
    def length(self):
        with self._lock:
            return len(self._list)
 
    def remove(self, value):
        with self._lock:
            return self._list.remove(value)

    def slice_list(self, left_range=0, right_range=0):
        with self._lock:
            self._list = self._list[left_range:right_range + 1]

    def __iter__(self):
        self.i = -1
        return self

    def __next__(self):
        with self._lock:
            if self.i < len(self._list)-1:
                self.i += 1
                return self._list[self.i]
            else:
                raise StopIteration


class ThreadSafeDict:
    def __init__(self):
        self._dict = dict()
        self._lock = Lock()
 
    def __getitem__(self, key):
        with self._lock:
            return self._dict[key]

    def __setitem__(self, key, value):
        with self._lock:
            self._dict[key] = value

    def __delitem__(self, key):
        with self._lock:
            del self._dict[key]

    def __contains__(self, key):
        with self._lock:
            return key in self._dict

    def keys(self):
        with self._lock:
            return list(self._dict.keys())

    def values(self):
        with self._lock:
            return list(self._dict.values())

    def items(self):
        with self._lock:
            return list(self._dict.items())

    def get(self, key, default=None):
        with self._lock:
            return self._dict.get(key, default)

    def clear(self):
        with self._lock:
            self._dict.clear()

    def copy(self):
        with self._lock:
            return self._dict.copy()

    def update(self, other_dict):
        with self._lock:
            self._dict.update(other_dict)

    def pop(self, key, default=None):
        with self._lock:
            return self._dict.pop(key, default)

    def popitem(self):
        with self._lock:
            return self._dict.popitem()

    def __len__(self):
        with self._lock:
            return len(self._dict)

    def __iter__(self):
        with self._lock:
            self._iter_keys = iter(self._dict)
            return self

    def __next__(self):
        with self._lock:
            return next(self._iter_keys)