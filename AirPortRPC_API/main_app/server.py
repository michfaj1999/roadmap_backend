import threading
from threading import Lock, Event
import socket as s
import json
import time
import select


class Server:
    def __init__(self, HOST, PORT, SocketPool, ConnectionPool, Airport, Visualization):
        self.HOST = HOST
        self.PORT = PORT
        self.SocketPool = SocketPool
        self.ConnectionPool = ConnectionPool
        self.Airport = Airport
        self.Visualization = Visualization
        self.Server_lock = Lock()
        self.Lobby_lock = Lock()
        self.start_time = time.monotonic()   

        self.pause_event = Event()
        self.pause_event.set()
        self.shutdown_event = Event()
        self.shutdown_event.set()
        self.lobby_socket = None
        self.stop_flag = False
        self.Threads = []
        self.Lobby = None
        self.Visualization_thread = None
        self.Runway_control = None

    def Lobby_thread(self):
        self.lobby_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
        self.lobby_socket.setsockopt(s.SOL_SOCKET, s.SO_REUSEADDR, 1)
        self.lobby_socket.bind((self.HOST, self.PORT))
        self.lobby_socket.listen()
        self.lobby_socket.setblocking(0)

        while self.shutdown_event.is_set():
            if not self.shutdown_event.is_set():
                return
            self.pause_event.wait()
            ready_to_read, _, _ = select.select([self.lobby_socket], [], [], 1)
            if ready_to_read:
                client_socket, adress = self.lobby_socket.accept()
                print("request accepted:{}".format(adress))
                with self.Lobby_lock:
                    if self.SocketPool.sockets_in_use.length() > 0:
                        port = self.SocketPool.sockets_in_use.get(0).PORT
                    else:
                        if self.SocketPool.avalible_ports.length() > 0:
                            print("Adding another port")
                            self.SocketPool.add_listening_socket()
                            port = self.SocketPool.sockets_in_use.get(0).PORT

                        else:
                            print("Connection error.No ports avalible.")
                            client_socket.send(b"Max connections reached")
                            client_socket.close()
                            continue
                    client_socket.send(json.dumps(port, ensure_ascii=False).encode("utf8"))
                    client_socket.setblocking(True)
                    response = client_socket.recv(1024)
                    client_socket.setblocking(False)
                    if response == b"True":
                        thread = threading.Thread(target=self.Server_thread)
                        thread.start()
                        self.Threads.append(thread)
                        client_socket.close()

    def Server_thread(self):
        with self.Server_lock:
            self.pause_event.wait()
            socket = self.SocketPool.get_socket()
            database_connection = self.ConnectionPool.get_connection()
        if socket is None or database_connection is None:
            return
        else:
            with self.Server_lock:
                self.pause_event.wait()
                socket.accept_client_socket()
                plane_parameters = self.Airport.prepare_plane_parameters(socket.client_socket, database_connection)
                self.Airport.add_plane_to_currently_operating(plane_parameters)
                self.Airport.send_plane_id_to_plane(plane_parameters)
                self.Airport.add_airplane_to_runway_queue_by_start_position(plane_parameters)
            self.Airport.wait_for_landing(plane_parameters, self.pause_event, self.shutdown_event)
            self.Airport.fly_to_final_approach_starting_point(plane_parameters, self.pause_event, self.shutdown_event)
            self.Airport.landing(plane_parameters, self.pause_event, self.shutdown_event)
            if not self.shutdown_event.is_set():
                return
            self.pause_event.wait()
            self.SocketPool.return_free_port_number_to_avalible_ports(socket.PORT)
            self.ConnectionPool.release_connection(database_connection)

    def start(self):
        print("Starting the Server")
        self.Lobby = threading.Thread(target=self.Lobby_thread)
        self.Runway_control = threading.Thread(target=self.Airport.runway_queues_control_loop, args=[self.pause_event, self.shutdown_event])
        self.Visualization_thread = threading.Thread(target=self.Visualization.VisuThread, args=[self.pause_event, self.shutdown_event])
        self.Lobby.start()
        self.Runway_control.start()
        self.Visualization_thread.start()
    
    def shutdown(self):
        print("Shutting down the Server...")
        self.shutdown_event.clear()
        for thread in self.Threads:
            thread.join()
        self.Lobby.join()
        if self.lobby_socket:
            self.lobby_socket.close()
        self.Visualization_thread.join()
        self.Runway_control.join()
        del self.Airport
        del self.Visualization
        del self.SocketPool
        del self.ConnectionPool
        
    def get_currently_operating_clients_count(self):
        return self.Airport.get_currently_operating_airplanes_count()
    
    def get_collisions_historical_data(self):
        return self.Airport.get_collisions_historical_data()
    
    def get_landing_historical_data(self):
        return self.Airport.get_landing_historical_data()
    
    def get_currently_operating_id_list(self):
        return self.Airport.get_currently_operating_airplanes_id_list()
    
    def get_plane_info_by_plane_id(self, plane_id):
        return self.Airport.get_plane_parameters_by_plane_id(plane_id)
    
    def uptime(self):
        return round(time.monotonic() - self.start_time, 2)    
