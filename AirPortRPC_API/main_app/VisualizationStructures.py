from main_app.ThreadSafeStructures import ThreadSafeDict
import pygame
from threading import Lock


class Visu:
    
    def __init__(self):
        self.currently_operating_airplanes = ThreadSafeDict()
        self.airplane_avatar = 'main_app/images/airplane.png'   
        self.airplane_avatar_x_size = 46
        self.airplane_avatar_y_size = 46
        self.airplane_z_scale_parameter = 20
        self.Visu_Lock = Lock()

    def add_airplane_to_visu(self, plane_parameters):
        position = self.convert_coordinates_for_visu(plane_parameters.position)
        plane_id = plane_parameters.plane_id
        airplane_surface = pygame.image.load(self.airplane_avatar)
        dimension = (self.airplane_avatar_x_size, self.airplane_avatar_y_size)      
        self.currently_operating_airplanes[plane_id] = [position, airplane_surface, dimension,]

    def update_airplane_position(self, plane_parameters):
        self.currently_operating_airplanes[plane_parameters.plane_id][0] = self.convert_coordinates_for_visu(plane_parameters.position)
    
    def scale_airplane_avatar_size_by_altitude(self, plane_parameters):
        max_altitude = 500
        z_position = int(plane_parameters.position[2]/10)
        x_dimension = self.airplane_avatar_x_size + int(self.airplane_z_scale_parameter * (z_position/max_altitude))
        y_dimension = self.airplane_avatar_y_size + int(self.airplane_z_scale_parameter* (z_position/max_altitude))                       
        self.currently_operating_airplanes[plane_parameters.plane_id][2] = (x_dimension, y_dimension)

    def delete_airplane_from_visu(self, plane_parameters):
        del self.currently_operating_airplanes[plane_parameters.plane_id]
        
    def convert_coordinates_for_visu(self, position):
        x = int(position[0]/10)
        y = int(position[1]/10)
       
        return (x, y)
    
    def VisuThread(self, pause_event, shutdown_event):

        with self.Visu_Lock:
            pygame.init()
            window = pygame.display.set_mode((1000, 1000))
            pygame.display.set_caption("Airport")
            clock = pygame.time.Clock()
            background_surface = pygame.Surface((1000, 1000))
            background_surface.fill("Green")
            # Lake_surface = pygame.image.load('images/lake.png')
            # Lake_surface  = pygame.transform.scale(Lake_surface, (700, 520))
            # Lake_surface = pygame.transform.rotate(Lake_surface, 90)
            # Mountains_surface = pygame.image.load('images/Mountains.png')
            # Mountains_surface = pygame.transform.rotate(Mountains_surface, 90)
            # Building_surface = pygame.image.load('images/Building.png')
            # Building_surface = pygame.transform.scale(Building_surface, (320, 200))
            # Tower_surface = pygame.image.load('images/Tower.png')
            # Tower_surface = pygame.transform.scale(Tower_surface, (150, 150))
            runway_A_surface = pygame.Surface((10, 200))
            runway_A_surface.fill("Gray")
            runway_B_surface = pygame.Surface((10, 200))
            runway_B_surface.fill("Gray")
            runway_A_final_approach_surface = pygame.Surface((10, 700))
            runway_A_final_approach_surface.fill((0, 240, 0))
            runway_B_final_approach_surface = pygame.Surface((10, 700))
            runway_B_final_approach_surface.fill((0, 240, 0))

        while shutdown_event.is_set():
            pause_event.wait()
            with self.Visu_Lock:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        exit()
                window.blit(background_surface, (0, 0))
                window.blit(runway_A_surface, (490, 800))
                window.blit(runway_B_surface, (510, 800))
                window.blit(runway_A_final_approach_surface, (490, 100))
                window.blit(runway_B_final_approach_surface, (510, 100))
                # window.blit(Lake_surface, (-90, 00))
                # window.blit(Mountains_surface, (800, 0))
                # window.blit(Building_surface, (530, 740))
                # window.blit(Tower_surface, (600, 540))
                for airplane in self.currently_operating_airplanes.keys():
                    airplane_model = self.currently_operating_airplanes[airplane] 
                    airplane_model[1] = pygame.transform.smoothscale(airplane_model[1],airplane_model[2])
                    airplane_model[1].unlock()
                    window.blit(airplane_model[1],airplane_model[0])
                pygame.display.update()
                clock.tick(15)