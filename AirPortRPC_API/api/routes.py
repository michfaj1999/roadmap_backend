from flask import jsonify, request


def configure_endpoint(app):
    def start_server():
        app.config['Server'].start()
        return "Server is running"

    def freeze_server():
        app.config['Server'].pause_event.clear()
        return "Server is frozen"

    def continue_server():
        app.config['Server'].pause_event.set()
        return "Server continues"

    def get_uptime():
        return app.config['Server'].uptime()

    def get_currently_operating_airplanes_count():
        return app.config['Server'].get_currently_operating_clients_count()

    def get_collisions_historical_data():
        return app.config['Server'].get_collisions_historical_data()

    def get_landing_historical_data():
        return app.config['Server'].get_landing_historical_data()

    def get_currently_operating_id_list():
        return app.config['Server'].get_currently_operating_id_list()

    def get_plane_info_by_plane_id(plane_id):
        return app.config['Server'].get_plane_info_by_plane_id(plane_id)

    def stop_server():
        app.config['Server'].shutdown()
        return "Server has been shut down"

    @app.route('/action', methods=['POST'])
    def action():
        try:
            data = request.get_json()

            method = data.get('method')
            params = data.get('params', {})

            methods = {
                'start': start_server,
                'freeze': freeze_server,
                'restore': continue_server,
                'uptime': get_uptime,
                'count': get_currently_operating_airplanes_count,
                'history_collisions': get_collisions_historical_data,
                'history_landed': get_landing_historical_data,
                'list': get_currently_operating_id_list,
                'plane_info': get_plane_info_by_plane_id,
                'close': stop_server,
            }

            if method in methods:
                if method == 'plane_info':
                    # Plane info method requires a parameter
                    plane_id = params.get('plane_id')
                    if plane_id is None:
                        return jsonify({'error': 'Missing parameter: plane_id'}), 400
                    result = methods[method](plane_id)
                else:
                    result = methods[method](**params)
                return jsonify({'result': result})
            else:
                return jsonify({'error': 'Method not found'}), 400

        except Exception as e:
            return jsonify({'error': str(e)}), 500
        