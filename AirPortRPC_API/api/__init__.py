from flask import Flask
from .routes import configure_endpoint



def create_app(Server):
    app = Flask(__name__)
    app.config["Server"] = Server
    configure_endpoint(app)
    return app


def run_flask(Server):
    app = create_app(Server)
    app.run(port=5000)
