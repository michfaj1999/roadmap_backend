# Roadmap_Backend

This project consists of programms made by me for "By the Hand" mentoring program.
In this program participants get only requirments for tasks,programs are made by their own.
Tasks include basics of Backend such as Client-server applications,working with database,concurrency etc.

1.Client-server - Simple Client-Server application for sending and receiving messages.

v.1.1.0 and v.1.1.1 - contains only basic server functionalities

v1.1.2 - contains list of users and their passwords and list of user's messageboxes,all saved in json files

v1.1.4 - contains simple PostGreSQL database with users and their messageboxes.

v.1.1.4.2 - version with SQLite database

2.Connection_Pool - Contains my own implementation of Connection Pooling using threading and PostGreSQL database.

3.MiniHackaton - Tasks from MiniHackaton for participants of "By the Hand" mentoring program.

Task 1 - Priority Queue - My own implementation of the Priority Queue in Python

Task 2 - Hash Table - My own implementation of Hash table in Python

Task 3 - Linked List - My own implementation of Linked List in Python 

4.Airport - Client-Server application which simulates Airport.The Server is an Airport and Clients are Airplanes.
Airport task is to handle multiply connections of many Airplanes and lead each Airplane to final approach path and
to the runway by sending proper messages via communication protocol.Application is based on Client-Server architecture 
using Sockets and provides handling of many clients via multithreading.Event logs are stored in SQLite database.
Visualization of an Airport is prepared in Pygame.
VIDEO LINK:
https://youtu.be/1HVYA7EaQ0g

5.AirportRestAPI - Airport version controlled by REST API created in Flask
